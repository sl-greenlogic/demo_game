# Changes in game for implementation with Enriched Wrapper

There is a description of integration `Enriched Wrapper` (`EW`) with existing prototype `game`. All pathes are relative to root of the `game` folder.

---
## How run game with wrapper

1. Make npm i
2. run command npm run start
3. set playerId to opened in a browser url as query param example: http://localhost:8000/?playerId=some_player_id 

---

## How set player ID

Add to url query param playerId={some_id} example: http://localhost:8000/?playerId=some_player_id

Player ID you can get from your stakelogic manger (BO Game Platform)

---

## New files

---

There were created a few new files, that are for work with `EW` only.

`/src/ExternalSlotController.ts`:

* created this new file: for work with `EW`
* Have functionality to read and update data from `Model` (`updateModel`, `onModelUpdate`)
* Have functionality to communicate with `EW` (subscribing in `constructor` to `EW` events, or emiting UPDATE_MODEL event in method `onModelUpdate`)
* Have functionality to handle button events (see `handlers`)
* Have additional usefull methods fro interaction with `game` and `EW` (`initUI`, `setAssets`, `syncState` etc.)

`/src/model/IFSM.ts`:

* created this new file: for work with `EW`
* Interfaces for `Controller` and `Model`
* describe types of values in parameters and properties of `Controller` and `Model`

`/src/model/SlotModel.ts`:

* created this new file: for work with `EW`
* Have description of all states, that are used for communication with `EW`
* Have some usefull functions like `parseByBuild` and `getJsonFromUrl` to get current location and transmit it to `EW`

---

## Added assets

---

There awere also added some folders and images for `EW`

`/assets`:

* added folders `paytable` and `rules` with images for those pages
* added image of menus background
* all images are added for using in `EW`

---

## Changed files

---

Some of the files were changed to follow common `game` logic, that is integrated with `EW`.

`/src/app.ts`:

* minRatio to 16 / 9 instead of 4 / 3
* use `ExternalSlotController.getInstance()` in the end of init method
* don't use `App.sceneController.loadScene(Scenes.BootScene, SceneLayer.UI)` in the end of init method and put it into separate static public method `static uiInit()`

`/src/index.ts`:

* don't use `App.application;` at the beginning
* correct parameters for `gcw.api.GameEventType.TOTAL_BET_UPDATE` event
* get parameters from server initial responces
* update `Model` (send them eventualy to `EW`) with server parameters using  `ExternalSlotController.getInstance().updateModel(parameters)`

`/src/components/tropical.ts`:

* added `public startSpin` (update bet, subscribe to `gcw.api.GameEventType.ROUND_REQUEST_CONFIRMED` event and emit `gcw.api.GameEventType.ROUND_REQUEST`)
* added `GcwEventsController.emitGcwEvent("CHANGE_STATE", { type: "SPINNING" });` to `onInitSpin` method
* set correct `denom` property to `gcw.api.GameEventType.NGS_REQUEST` parameter (it should be `denom: ExternalSlotController.getInstance().getParameter('bet') * ExternalSlotController.getInstance().getParameter('totalBetMultiplier') * 1000`)
* in `onStop` method update state `GcwEventsController.emitGcwEvent("CHANGE_STATE", { type: "IDLE" });` and `Model` state `ExternalSlotController.getInstance().updateModelWalletToLastState();`

`/src/componentsui/gameControls.ts`:

* remove quatro functionality

`/src/scenes/game.scene.ts`:

* added in method `onFsmStarted` passing of game instance into ExternalSlotController `ExternalSlotController.getInstance().setGame(this.game);`

`/src/scenes/preloader.scene.ts`:

* added into method `onLoadProgressChange` global event dispatch for event `preloader.step` with data of percentage for `EW` (`document.dispatchEvent(new CustomEvent("preloader.step", { detail: { stage: "game", percent: progress.toFixed(0) } }));`)
* added into method `onLoadComplete` global event dispatch for event `preloader.complete` with data (`document.dispatchEvent(new CustomEvent("preloader.complete", { detail: { stage: "game", percent: 100 } }));`)

`/src/sdk/core/slotmachine.ts`:

* added muting of sounds `Howler.mute(true);` because it is common behaviour for games (no sounds on start)

`/src/sdk/core/gametypes/slotgame/slotGame.ts`:

* added `startSpin(): void;` method into interface of `SlotGame` class

`/src/sdk/core/gcw/gcwEventsController.ts`:

* added update of `Model` in initial responces using `ExternalSlotController.getInstance().updateModelFromMessage(data);`

`/src/sdk/core/gcw/gcwGameClient.ts`:

* ensure that `GcwEventsController.setEventEmitter` will be used only once to avoid of duplicating events and issues related to it
* `App.initialized` is indicating using of `GcwEventsController.setEventEmitter`
