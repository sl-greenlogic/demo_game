/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { Button, ButtonStateTexture, ButtonState } from '../sdk/ui/button';
import { SlotMachine, GameStates } from '../sdk/core/slotmachine';
import {
  Resources,
  audioSpriteSheetRes,
  loadedFiles,
  atlasRes,
} from '../settings/game.resources';
import { GcwEventsController } from '../sdk/core/gcw/gcwEventsController';
import { Howl } from 'howler';

export class SpinButton extends Button {
  private texturesStates: ButtonStateTexture[] = [
    {
      state: ButtonState.Active,
      gameState: [GameStates.Ready],
      texture: 'bttn_spin_round.png',
    },
    {
      state: ButtonState.HoverOver,
      gameState: [GameStates.Ready],
      texture: 'bttn_spin_round_over.png',
    },
    {
      state: ButtonState.HoverOut,
      gameState: [GameStates.Ready],
      texture: 'bttn_spin_round.png',
    },
    {
      state: ButtonState.Click,
      gameState: [GameStates.Ready],
      texture: 'bttn_spin_round_push.png',
    },
    {
      state: ButtonState.Release,
      gameState: [GameStates.Ready],
      texture: 'bttn_spin_round_inactive.png',
    },
    {
      state: ButtonState.ReleaseOutside,
      gameState: [GameStates.Ready],
      texture: 'bttn_spin_round.png',
    },

    {
      state: ButtonState.Active,
      gameState: [
        GameStates.InitRound,
        GameStates.PerformRound,
        GameStates.PreStopRound,
      ],
      texture: 'bttn_spin_round_active.png',
    },
    {
      state: ButtonState.HoverOver,
      gameState: [
        GameStates.InitRound,
        GameStates.PerformRound,
        GameStates.PreStopRound,
      ],
      texture: 'bttn_spin_round_over.png',
    },
    {
      state: ButtonState.HoverOut,
      gameState: [
        GameStates.InitRound,
        GameStates.PerformRound,
        GameStates.PreStopRound,
      ],
      texture: 'bttn_spin_round_active.png',
    },
    {
      state: ButtonState.Click,
      gameState: [
        GameStates.InitRound,
        GameStates.PerformRound,
        GameStates.PreStopRound,
      ],
      texture: 'bttn_spin_round_push.png',
    },
    {
      state: ButtonState.Release,
      gameState: [
        GameStates.InitRound,
        GameStates.PerformRound,
        GameStates.PreStopRound,
      ],
      texture: 'bttn_spin_round_inactive.png',
    },
    {
      state: ButtonState.ReleaseOutside,
      gameState: [
        GameStates.InitRound,
        GameStates.PerformRound,
        GameStates.PreStopRound,
      ],
      texture: 'bttn_spin_round_active.png',
    },
  ];

  private clickSpinId!: number;
  private clickSpinSnd!: Howl;

  constructor() {
    super();

    this.clickSpinId = 0;
    this.clickSpinSnd = Resources.audios[audioSpriteSheetRes.sfx_menu].audio;
    this.texture =
      loadedFiles[atlasRes.ui_controls_buttons].textures['bttn_spin_round.png'];
    SlotMachine.subscribeForFsmStatusChange({
      state: GameStates.Any,
      callback: this.onSlotMachineUpdateStatus.bind(this),
      active: true,
    });
  }

  public enableInteraction(): void {
    this.on('pointerdown', () => {
      this.onPointerDown();
    });
    this.on('pointerup', () => {
      this.onPointerUp();
    });
    this.on('pointerout', () => {
      this.onPointerOut();
    });
    this.on('pointerover', () => {
      this.onPointerOver();
    });
    this.interactive = true;
  }

  public disableInteraction(): void {
    this.interactive = false;
  }

  public onPointerDown(): void {
    for (let index = 0; index < this.texturesStates.length; index++) {
      const textureState = this.texturesStates[index];
      if (
        textureState.state === ButtonState.Click &&
        textureState.gameState.includes(SlotMachine.fsmState)
      ) {
        this.texture =
          loadedFiles[atlasRes.ui_controls_buttons].textures[
            textureState.texture
          ];
        break;
      }
    }
  }
  private onPointerUp(): void {
    for (let index = 0; index < this.texturesStates.length; index++) {
      const textureState = this.texturesStates[index];
      if (
        (textureState.state === ButtonState.Release ||
          textureState.state === ButtonState.ReleaseOutside) &&
        textureState.gameState.includes(SlotMachine.fsmState) &&
        textureState.texture
      ) {
        this.texture =
          loadedFiles[atlasRes.ui_controls_buttons].textures[
            textureState.texture
          ];
        break;
      }
    }
    if (SlotMachine.fsmState === GameStates.Ready) {
      GcwEventsController.emitGcwEvent(
        gcw.api.GameEventType.TOTAL_BET_UPDATE,
        0.2
      );
      GcwEventsController.subscribeForGcwEvent(
        gcw.api.GameEventType.ROUND_REQUEST_CONFIRMED,
        () => {
          SlotMachine.changeState(GameStates.InitRound);

          if (this.clickSpinId === 3) this.clickSpinId -= 2;
          else this.clickSpinId += 1;

          this.clickSpinSnd.stop();
          this.clickSpinSnd.play('sfx_startSpin' + this.clickSpinId);
        }
      );
      GcwEventsController.emitGcwEvent(gcw.api.GameEventType.ROUND_REQUEST);
    }
  }
  private onPointerOver(): void {
    for (let index = 0; index < this.texturesStates.length; index++) {
      const textureState = this.texturesStates[index];
      if (
        textureState.state === ButtonState.HoverOver &&
        textureState.gameState.includes(SlotMachine.fsmState) &&
        textureState.texture
      ) {
        this.texture =
          loadedFiles[atlasRes.ui_controls_buttons].textures[
            textureState.texture
          ];
        break;
      }
    }
  }
  private onPointerOut(): void {
    for (let index = 0; index < this.texturesStates.length; index++) {
      const textureState = this.texturesStates[index];
      if (
        textureState.state === ButtonState.HoverOut &&
        textureState.gameState.includes(SlotMachine.fsmState) &&
        textureState.texture
      ) {
        this.texture =
          loadedFiles[atlasRes.ui_controls_buttons].textures[
            textureState.texture
          ];
        break;
      }
    }
  }

  private onSlotMachineUpdateStatus(): void {
    for (let index = 0; index < this.texturesStates.length; index++) {
      const textureState = this.texturesStates[index];
      if (
        textureState.state === ButtonState.HoverOut &&
        textureState.gameState.includes(SlotMachine.fsmState) &&
        textureState.texture
      ) {
        this.texture =
          loadedFiles[atlasRes.ui_controls_buttons].textures[
            textureState.texture
          ];
        break;
      }
    }
  }
}
