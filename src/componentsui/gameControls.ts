/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { QuatroBtn } from './quattrobutton';
import { GameControl } from '../sdk/ui/gamecontrol';

export class GameControls extends PIXI.Container implements GameControl {
  // private spinButton!: Button;
  public currentQuattroBtn!: QuatroBtn;

  constructor(scene: any) {
    super();
  }

  addControlsToScene(): void {}
}
