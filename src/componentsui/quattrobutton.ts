/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { Button } from '../sdk/ui/button';
import { TweenMax } from 'gsap';
import { Resources, audioSpriteSheetRes } from '../settings/game.resources';

export class QuatroBtn extends Button {
  constructor(
    private _reelSetsAmount: number,
    private enabledTexture: PIXI.Texture,
    private disabledTexture: PIXI.Texture,
    callBackOnClick: Function,
    startActivated = false
  ) {
    super();

    if (!startActivated) this.disable();
    else this.enable();

    this.interactive = true;
    this.on('pointerdown', () => {
      let audioId = 0;

      if (this.reelSetsAmount < 4)
        audioId = Resources.audios[audioSpriteSheetRes.sfx_menu].audio.play(
          'sfx_clickAllOtherClicks'
        );
      else
        audioId = Resources.audios[audioSpriteSheetRes.sfx_menu].audio.play(
          'sfx_activateQuattro'
        );

      Resources.audios[audioSpriteSheetRes.sfx_menu].audio.volume(0.4, audioId);

      callBackOnClick(this);
    });
  }

  public enable(): void {
    this.texture = this.enabledTexture;

    if (this.parent) {
      let cloneSprite = new PIXI.Sprite(this.texture);
      cloneSprite.anchor.set(0.5);
      cloneSprite.position.set(this.x, this.y);
      this.parent.addChild(cloneSprite);
      TweenMax.to(cloneSprite, 1, {
        alpha: 0,
        onUpdate: () => {
          cloneSprite.y -= 0.5;
        },
        onComplete: () => {
          cloneSprite.destroy();
        },
      });
    }
    super.enable();
  }
  public disable(): void {
    this.texture = this.disabledTexture;
    super.disable();
  }

  get reelSetsAmount(): number {
    return this._reelSetsAmount;
  }

  public enableInteraction(): void {}
  public disableInteraction(): void {}
}
