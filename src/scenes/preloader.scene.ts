/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { SceneLayer } from '../sdk/core/controllers/sceneController';
import {
  ScreenSettings,
  GameSettings,
  Scenes,
} from '../settings/game.settings';
import { App } from '../app';
import { Resources } from '../settings/game.resources';
import 'howler';
import { Scene } from '../sdk/core/basecomponents/scene';
import { GcwEventsController } from '../sdk/core/gcw/gcwEventsController';

export class PreLoaderScene extends Scene {
  private title!: PIXI.Text;
  private loadedBar!: PIXI.Sprite;
  private sizeBarPct!: number;
  private barStartPosX!: number;

  constructor(layer: SceneLayer) {
    super(layer);
    this.create();
  }

  protected create(): void {
    const titleStyle = new PIXI.TextStyle({
      fontFamily: 'neuropolitical',
      fontWeight: 'bold',
      fontSize: 20,
      fill: '#00ff00',
    });

    this.title = new PIXI.Text('Please Wait.', titleStyle);
    this.title.anchor.set(0.5);
    this.sceneContainer.addChild(this.title);
    this.title.x = ScreenSettings.Center.x;
    this.title.y = ScreenSettings.Center.y - 30;

    let bar = new PIXI.Sprite(PIXI.loader.resources['loading_bar'].texture);
    bar.anchor.set(0.5);
    bar.x = ScreenSettings.Center.x;
    bar.y = ScreenSettings.Center.y - 50;
    bar.tint = 0xcccccc;
    this.sceneContainer.addChild(bar);

    let barMask = new PIXI.Sprite(PIXI.loader.resources['loading_bar'].texture);
    barMask.anchor.set(0.5);
    barMask.x = ScreenSettings.Center.x;
    barMask.y = ScreenSettings.Center.y - 50;
    this.sceneContainer.addChild(barMask);

    this.loadedBar = new PIXI.Sprite(
      PIXI.loader.resources['loading_bar'].texture
    );
    this.loadedBar.anchor.set(0.5);

    this.loadedBar.x = ScreenSettings.Center.x - barMask.width;
    this.loadedBar.y = ScreenSettings.Center.y - 50;
    this.loadedBar.tint = 0x00ff00;
    this.loadedBar.mask = barMask;
    this.sceneContainer.addChild(this.loadedBar);

    this.sizeBarPct = this.loadedBar.width / 100;
    this.barStartPosX = this.loadedBar.x;

    Resources.loadResources(
      this.onLoadProgressChange.bind(this),
      this.onLoadComplete.bind(this)
    );
  }

  private onLoadProgressChange(progress: number): void {
    GcwEventsController.emitGcwEvent(
      gcw.api.GameEventType.PRELOADING_PROGRESS,
      progress.toFixed(0)
    );
    this.loadedBar.x = this.barStartPosX + this.sizeBarPct * progress;
    this.title.text = progress.toFixed(0) + '%';

    GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      general: {
        preloaderStep: progress.toFixed(0),
      },
    });
  }

  private onLoadComplete(): void {
    GcwEventsController.emitGcwEvent(
      gcw.api.GameEventType.PRELOADING_PROGRESS,
      100
    );
    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.PRELOADING_END);
    this.loadedBar.x = this.barStartPosX + this.sizeBarPct * 100;
    this.title.text = '100%';

    GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      general: {
        preloaderComplete: true,
      },
    });
    if (GameSettings.ShowIntro) {
      App.sceneController.loadScene(Scenes.IntroScene, SceneLayer.UI);
    } else {
      App.sceneController.loadScene(Scenes.GameScene, SceneLayer.GAME);
    }
  }

  public update(delta: number): void {}

  public resize(): void {}
}
