/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */
import { TweenMax, Back, Linear } from 'gsap';
import { BLEND_MODES } from 'pixi.js';
import { SceneLayer } from '../sdk/core/controllers/sceneController';
import { ScreenSettings, Scenes } from '../settings/game.settings';
import * as particles from 'pixi-particles';
import {
  loadedFiles,
  imagesRes,
  atlasRes,
  spineRes,
} from '../settings/game.resources';
import { App } from '../app';
import { Scene } from '../sdk/core/basecomponents/scene';

export class IntroScene extends Scene {
  private gameTitle!: PIXI.spine.Spine;
  private continueBtn!: PIXI.Sprite;

  constructor(layer: SceneLayer) {
    super(layer);
    this.create();
  }

  protected create(): void {
    const background = new PIXI.Sprite(
      loadedFiles[imagesRes.background_introscreen].texture
    );
    background.anchor.set(0.5);
    background.position.set(ScreenSettings.Center.x, ScreenSettings.Center.y);
    this.sceneContainer.addChild(background);

    const stakeProduction = new PIXI.Sprite(
      loadedFiles[atlasRes.game_parts].textures['AStakelogicProduction.png']
    );
    stakeProduction.anchor.set(0.5);
    stakeProduction.position.set(ScreenSettings.Center.x - 388, 11);
    this.sceneContainer.addChild(stakeProduction);

    this.gameTitle = new PIXI.spine.Spine(
      loadedFiles[spineRes.logo_spine].spineData
    );
    this.gameTitle.scale.set(0.7);
    this.gameTitle.x = ScreenSettings.Center.x;
    this.gameTitle.y = -200;
    this.gameTitle.state.setAnimation(0, 'logo_idle', true);
    this.gameTitle.state.timeScale = 0.2;
    this.sceneContainer.addChild(this.gameTitle);

    this.continueBtn = new PIXI.Sprite(
      loadedFiles[atlasRes.ui_controls_buttons].textures['totalBet_display.png']
    );
    this.continueBtn.anchor.set(0.5);
    this.continueBtn.x = ScreenSettings.Center.x;
    this.continueBtn.y =
      ScreenSettings.Size.height - (ScreenSettings.Size.height / 100) * 30;
    this.continueBtn.alpha = 0;

    const titleStyle = new PIXI.TextStyle({
      fontFamily: 'neuropolitical',
      fontWeight: 'bold',
      fontSize: 20,
      fill: '#51dd49',
    });
    let continueText = new PIXI.Text('CONTINUE', titleStyle);
    continueText.anchor.set(0.5);
    this.continueBtn.addChild(continueText);

    this.sceneContainer.addChild(this.continueBtn);

    this.continueBtn.on('pointerdown', () => {
      this.continueBtn.blendMode = BLEND_MODES.NORMAL;
    });
    this.continueBtn.on('pointerup', () => {
      this.continueBtn.interactive = false;
      this.continueBtn.removeAllListeners();
      this.startTheGame();
    });
    this.continueBtn.on('pointerout', () => {
      this.continueBtn.blendMode = BLEND_MODES.NORMAL;
    });
    this.continueBtn.on('pointerover', () => {
      this.continueBtn.blendMode = BLEND_MODES.ADD;
    });

    this.continueBtn.on('pointerdown', () => {
      this.continueBtn.blendMode = BLEND_MODES.ADD;
    });

    TweenMax.to(this.gameTitle, 1, {
      y: 75,
      ease: Back.easeOut,
      delay: 0.5,
      onComplete: () => {
        TweenMax.to(this.continueBtn, 1, { alpha: 1, ease: Linear.easeNone });

        this.continueBtn.interactive = true;
        this.continueBtn.buttonMode = true;
        this.particleEffectTitle(this.gameTitle);
      },
    });
  }

  private particleEffectTitle(container: PIXI.Container) {
    var emitter = new particles.Emitter(
      container,
      [imagesRes.smokeparticle, imagesRes.particle],
      {
        alpha: {
          start: 0.4,
          end: 0,
        },
        scale: {
          start: 0.5,
          end: 0.1,
          minimumScaleMultiplier: 1,
        },
        color: {
          start: '#9af31a',
          end: '#7cc73c',
        },
        speed: {
          start: 10,
          end: 10,
          minimumSpeedMultiplier: 1,
        },
        acceleration: {
          x: 0,
          y: 0,
        },
        maxSpeed: 0,
        startRotation: {
          min: 0,
          max: 360,
        },
        noRotation: false,
        rotationSpeed: {
          min: 0,
          max: 0,
        },
        lifetime: {
          min: 2,
          max: 1.8,
        },
        blendMode: 'add',
        frequency: 0.02,
        emitterLifetime: -1,
        maxParticles: 600,
        pos: {
          x: 0.5,
          y: 0.5,
        },
        addAtBack: false,
        spawnType: 'rect',
        spawnRect: {
          x: -500,
          y: -50,
          w: 1000,
          h: 100,
        },
      }
    );

    var elpsedEmitter = Date.now();
    let tickerParticle = new PIXI.ticker.Ticker();
    tickerParticle.add((deltaTime) => {
      var now = Date.now();
      emitter.update((now - elpsedEmitter) * 0.001);
      elpsedEmitter = now;
    });
    emitter.emit = true;
    tickerParticle.start();
  }

  public startTheGame(): void {
    setTimeout(() => {
      App.sceneController.loadScene(Scenes.GameScene, SceneLayer.GAME);
    }, 100);
  }

  public update(delta: number): void {}

  public resize(): void {}
}
