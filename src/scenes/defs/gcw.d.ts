declare module gcw.protocol {
  class MessageButtonCode {
    static FREE_ROUND_POSTPONE: string;
    static FREE_ROUND_FORFEIT: string;
  }
}
declare module gcw.protocol {
  interface Button {
    label: string;
    code?: any;
    link?: any;
  }
}
declare module gcw.protocol {
  class BrowserPlatform {
    static DESKTOP: string;
    static MOBILE: string;
    static MINIGAME: string;
  }
}
declare module gcw.protocol {
  interface Evt {
    type: string;
  }
}
declare module gcw.protocol {
  interface Configuration {
    game?: any;
    general?: any;
  }
}
declare module gcw.protocol {
  interface Message {
    code?: string;
    message?: string;
    externalCode?: string;
    externalMessage?: string;
    buttons?: Array<Button>;
  }
}
declare module gcw.protocol {
  interface Wallet {
    currencySymbol: string;
    currencyCode: string;
    decimalSpaces: number;
    rm: number;
    fm: number;
    bonuses: Array<{
      id: number;
      gsManaged: boolean;
      amount: number;
      priority: number;
      forfeitable: boolean;
    }>;
    freeRounds: {
      id: number;
      gsManaged: boolean;
      available: number;
      total: number;
      forfeitable: boolean;
      priority: number;
      totalWin: number;
      bet: number;
      roundConfig: any;
    };
  }
}
declare module gcw.protocol {
  interface ConfigurationRequest {
    init?: boolean;
    save?: Configuration;
  }
}
declare module gcw.protocol {
  class GameMode {
    static CREDITS: string;
    static MONEY: string;
  }
}
declare module gcw.protocol {
  class Resolution {
    static _1080P: string;
    static _540P: string;
  }
}
declare module gcw.protocol {
  class GamePlayMode {
    static COIN_BASED: string;
    static BET_BASED: string;
  }
}
declare module gcw.protocol {
  interface GameRequest {
    init: boolean;
    input: {
      event: Evt;
      currentWallet: Wallet;
    };
    confirm: {
      count: number;
      gameStepNo: number;
    };
    buttonCode: string;
    forfeitBonus: {
      id: number;
    };
  }
}
declare module gcw.protocol {
  interface GameResponse {
    gameStepNo: number;
    casinoGameRoundId: string;
    mode: string;
    events: Array<Evt>;
    numberOfNonAnimatedEvents: number;
    walletAfterLastBet: Wallet;
    currentWallet: Wallet;
    messagesBeforeGameEvents: Array<Message>;
    messagesAfterGameEvents: Array<Message>;
    jackpotWins: Array<{
      gameFeatureJackpot: boolean;
      valueInPlayersCurrency: number;
      value: number;
      currencySymbol: string;
      currencyCode: string;
      level: number;
    }>;
    casinoData?: any;
  }
}
declare module gcw.protocol {
  interface JackpotRequest {
    init: boolean;
    lastActualizationTimestamp: string;
  }
}
declare module gcw.protocol {
  interface JackpotResponse {
    timestamp: string;
    values: Array<{
      level: number;
      value: number;
    }>;
  }
}
declare module gcw.protocol {
  interface LogRequest {
    date: Date;
    sessionUrlId: string;
    message: string;
    stackTrace?: string;
    severity?: LoggingLevel;
  }
}
declare module gcw.protocol {
  class LoggingLevel {
    static TRACE: string;
    static DEBUG: string;
    static INFO: string;
    static WARN: string;
    static ERROR: string;
  }
}
declare module gcw.protocol {
  interface OperationRequest {
    init: boolean;
    initReplay: boolean;
    exit: boolean;
  }
}
declare module gcw.protocol {
  interface GameConfig {
    brand?: string;
    platform?: string;
    resolution?: string;
    mode?: string;
  }
}
declare module gcw.protocol {
  interface PlayerConfig {
    locale?: string;
    sounds?: boolean;
  }
}
declare module gcw.protocol {
  interface CasinoConfig {
    brand?: string;
  }
}
declare module gcw.protocol {
  interface OperationResponse {
    gameScriptUrl: string;
    game?: GameConfig;
    player?: PlayerConfig;
    casino?: CasinoConfig;
    closeGameClient: boolean;
    realityCheck?: any;
    loggingServiceUrl: string;
    gamebrand?: string;
    spinDuration?: number;
    ukgc?: boolean;
    enableportrait?: number;
    minimalui?: number;
    simpleBet?: number;
  }
}
declare module gcw.protocol {
  interface ReplayRequest {
    gameSessionReplay: boolean;
    gameRoundReplay?: {
      gameRoundReference: string;
    };
  }
}
declare module gcw.protocol {
  interface ReplayResponse {
    gameSession: {
      gameSessionId: number;
      playerId: string;
      gameRounds: Array<{
        reference: string;
        started: string;
        finished: string;
      }>;
    };
    gameRound: {
      gameSessionId: number;
      gameRoundId: number;
      playerId: string;
      initEvents: Array<Evt>;
      commLog: Array<{
        gameClientId: number;
        clientMessageId: number;
        dateProcessed: string;
        request: Request;
        response: Response;
      }>;
      automaticSteps: Array<{
        date: string;
        events: Array<Evt>;
        jackpotWins: Array<{
          gameFeatureJackpot: boolean;
          valueInPlayersCurrency: number;
          value: number;
          currencySymbol: string;
          currencyCode: string;
          level: number;
        }>;
      }>;
    };
  }
}
declare module gcw.protocol {
  interface StatusResponse {
    code: string;
    message: string;
    externalCode: string;
    externalMessage: string;
  }
}
declare module gcw.protocol {
  interface Request {
    id: number;
    date: string;
    game: GameRequest;
    operation: OperationRequest;
    configuration: ConfigurationRequest;
    jackpot: JackpotRequest;
    replay: ReplayRequest;
  }
}
declare module gcw.protocol {
  interface Response {
    id: number;
    date: string;
    status: StatusResponse;
    game: GameResponse;
    jackpot: JackpotResponse;
    operation: OperationResponse;
    configuration: Configuration;
    replay: ReplayResponse;
  }
}
declare module gcw.api {
  interface Game {
    setEventListener(eventListener: (type: string, data?: any) => void): void;
    sendEvent(type: string, data?: any): void;
  }
  interface ErrDecorator {
    getErrDlgStyle(): string;
  }
  interface GmApiFactory {
    getGmApi(realityCheck: RealityCheck): any;
  }
  interface InitializeData {
    containerElement: HTMLElement;
    gameSessionUrl: string;
    initConf: gcw.api.InitConf;
    replayConf?: gcw.api.ReplayConf;
  }
  interface InitConf {
    game?: gcw.protocol.GameConfig;
    player?: gcw.protocol.PlayerConfig;
    casino?: gcw.protocol.CasinoConfig;
    locale?: string;
    platform?: string;
    gamePlayMode?: string;
    brand?: string;
    gamebrand?: string;
    spinDuration?: number;
    ukgc?: boolean;
    enableportrait?: number;
    minimalui?: number;
    simpleBet?: number;
  }
  interface RealityCheck {
    interval: number;
    usedTime: number;
    buttonContinue: string;
    buttonExit: string;
    historyUrlLink: string;
    historyUrlText: string;
    message: string;
  }
  interface ReplayConf {
    gameRoundRef?: string;
  }
  interface Error {
    code: string;
    message: string;
    externalCode?: string;
    externalMessage?: string;
    data?: any;
  }
  interface GameLogRequestData {
    message: string;
    stackTrace: string;
    severity: gcw.protocol.LoggingLevel;
  }
  class GameEventType {
    static INITIALIZE: string;
    static INITIALIZE_SUCCESS: string;
    static INITIALIZE_FAILURE: string;
    static START: string;
    static START_SUCCESS: string;
    static START_FAILURE: string;
    static STOP: string;
    static STOP_SUCCESS: string;
    static STOP_FAILURE: string;
    static TERMINATE: string;
    static TERMINATE_SUCCESS: string;
    static TERMINATE_FAILURE: string;
    static HOME: string;
    static REGISTER: string;
    static CASHIER: string;
    static JACKPOT: string;
    static BIG_WIN: string;
    static MEGA_WIN: string;
    static BONUS_CONVERT: string;
    static OUT_OF_MONEY: string;
    static NGS_REQUEST: string;
    static NGS_RESPONSE: string;
    static NGS_ERROR: string;
    static NGS_LOG_REQUEST: string;
    static FREE_ROUND_POSTPONE: string;
    static PRELOADING_START: string;
    static PRELOADING_PROGRESS: string;
    static PRELOADING_END: string;
    static LOBBY_ACTIVE: string;
    static VERSION: string;
    static VERSION_RESPONSE: string;
    static CURRENCY_FORMATTING: string;
    static CURRENCY_FORMATTING_RESPONSE: string;
    static MUSIC_ACTIVE: string;
    static SOUND_EFFECTS_ACTIVE: string;
    static GENERAL_SOUND_STATUS_CHANGED: string;
    static MUSIC_STATUS_CHANGED: string;
    static SOUND_EFFECTS_STATUS_CHANGED: string;
    static GENERAL_SOUND_STATUS_CHANGE: string;
    static MUSIC_STATUS_CHANGE: string;
    static SOUND_EFFECTS_STATUS_CHANGE: string;
    static CONTROL_PANEL_STATUS_CHANGED: string;
    static ABOUT_STATUS_CHANGED: string;
    static PAY_TABLE_STATUS_CHANGED: string;
    static CONTROL_PANEL_STATUS_CHANGE: string;
    static ABOUT_STATUS_CHANGE: string;
    static PAY_TABLE_STATUS_CHANGE: string;
    static WALLET_UPDATED: string;
    static WALLET_UPDATE: string;
    static ROUND_START: string;
    static ROUND_ID: string;
    static ROUND_END: string;
    static TOTAL_WIN_UPDATE: string;
    static TOTAL_BET_UPDATE: string;
    static RESULTS_SHOWN: string;
    static ERROR: string;
    static ERROR_HANDLED: string;
    static ERROR_NOT_HANDLED: string;
    static AUTOPLAY_ACTIVATE: string;
    static AUTOPLAY_ACTIVATE_SUCCESS: string;
    static AUTOPLAY_ACTIVATE_FAILURE: string;
    static AUTOPLAY_DEACTIVATE: string;
    static AUTOPLAY_LIMIT_REACHED: string;
    static AUTOPLAY_CUSTOM_FEATURE: string;
    static AUTOPLAY_CUSTOM_FEATURE_SETTINGS: string;
    static ROUND_REQUEST: string;
    static ROUND_REQUEST_CONFIRMED: string;
    static GUI_BLOCKED: string;
    static GUI_BLOCKED_RESPONSE: string;
    static FULLSCREEN_MODE_ON: string;
    static FULLSCREEN_MODE_OFF: string;
    static FULLSCREEN_MODE_HANDLED: string;
    static FULLSCREEN_MODE_NOT_HANDLED: string;
    static COMPLIANCE_MODE: string;
  }
  class ErrorCode {
    static INVALID_PHASE: string;
    static CONNECTION_LOST: string;
    static FAILED_TO_LOAD_ASSETS: string;
    static UNSUPPORTED_BROWSER: string;
    static UNKNOWN: string;
    static errors: string[];
    static hasErrorCode(code: string): boolean;
    static isFatal(code: string): boolean;
  }
}
declare module gcw.gamebrand {
  class ErrDecorator implements gcw.api.ErrDecorator {
    getErrDlgStyle(): string;
  }
}
declare let leanderGMApi: any;
declare let gamePaused: boolean;
declare module gcw.gamebrand {
  class GmApiFactoryImpl implements gcw.api.GmApiFactory {
    getGmApi(realityCheck: gcw.api.RealityCheck): any;
  }
}
declare module gcw.adapter.lega {
  function loadLeanderGMApi(callback: (legaGMApi: Object) => void): void;
  function sessionRequest(
    legaGMApi: any,
    callback: (sessionId: string) => void
  ): void;
  function initSession(
    callback: (legaGMApi: any, sessionId: string) => void
  ): void;
  function initGameSessionUrl(
    legaUrl: any,
    onResponse: (
      gameSessionUrl: string,
      freePlaysData: any,
      isInterrupted: any
    ) => void,
    onError: (response: Object) => void
  ): void;
}
declare module gcw.adapter.gmapi {
  class Publications {
    static PRELOADING_STARTED: string;
    static PRELOADING_PROGRESS_UPDATED: string;
    static PRELOADING_ENDED: string;
    static GAME_VERSION_UPDATE: string;
    static GENERAL_SOUND_STATUS_CHANGED: string;
    static MUSIC_STATUS_CHANGED: string;
    static SOUND_EFFECTS_STATUS_CHANGED: string;
    static GENERAL_SOUND_STATUS_CHANGED_GAME: string;
    static MUSIC_STATUS_CHANGED_GAME: string;
    static SOUND_EFFECTS_STATUS_CHANGED_GAME: string;
    static CONTROL_PANEL_STATUS_CHANGED: string;
    static ABOUT_GAME_STATUS_CHANGED: string;
    static PAY_TABLE_STATUS_CHANGED: string;
    static CONTROL_PANEL_STATUS_CHANGED_GAME: string;
    static ABOUT_GAME_STATUS_CHANGED_GAME: string;
    static PAY_TABLE_STATUS_CHANGED_GAME: string;
    static BALANCE_UPDATED: string;
    static BALANCE_UPDATED_GAME: string;
    static GAME_STATUS_CHANGED: string;
    static SESSION_DATA_UPDATED: string;
    static INITIALIZED: string;
    static INITIALIZED_CONFIRMED: string;
    static JACKPOT_UPDATED: string;
    static TOTALWIN_UPDATED: string;
    static TOTALBET_UPDATED: string;
    static GAME_RESULTS_SHOWN: string;
    static ERROR_HANDLED: string;
    static ERROR_OCCURRED: string;
    static DISPOSE_APPLICATION: string;
    static GAME_BLOCKED_GUI: string;
    static GAME_LOADER_READY: string;
    static GAME_READY_FOR_UNLOAD: string;
    static AUTOPLAY: string;
    static GAME_RESULTS_LOOP_SHOWN: string;
    static GAME_UPDATED_DEBUG_OPTION_HISTORY: string;
    static GAME_UPDATED_PLAY_ID: string;
    static AUTOPLAY_ACTIVATED_GAME: string;
    static AUTOPLAY_ACTIVATED_CONFIRMATION: string;
    static AUTOPLAY_ACTIVATED_CANCELLATION: string;
    static AUTOPLAY_CANCELLED: string;
    static AUTOPLAY_LIMIT_REACHED: string;
    static AUTOPLAY_CUSTOM_FEATURE_IN_GAME: string;
    static AUTOPLAY_CUSTOM_FEATURE_SETTINGS: string;
    static PLAY_REQUEST: string;
    static PLAY_CONFIRMED: string;
    static FULLSCREEN_MODE_ON: string;
    static FULLSCREEN_MODE_OFF: string;
    static FULLSCREEN_MODE_CHANGED: string;
    static BROWSER_ORIENTATION_CHANGED: string;
    static BROWSER_RESIZED: string;
    static GOTO_LOBBY: string;
    static PLAYBET_UPDATED: string;
    static PLAYWIN_UPDATED: string;
    static PROMOTION_ACTIVE_PLAYS_DONE: string;
    static PROMOTION_ACTIVE_PLAYS_REMAINING: string;
    static PROMOTION_ACTIVE_UPDATED: string;
    static PROMOTION_CANCELLED: string;
    static PROMOTION_ACTIVE_WON_SO_FAR: string;
    static PROMOTION_FREE_PLAYS_REMAINING_UPDATED: string;
    static PROMOTION_SET_DATA: string;
    static PROMOTION_UPDATE_DATA: string;
    static SLOT_CONTROL_MODE_CHANGED: string;
    static TOTALBET_BYPASS_CHECK: string;
    static UPDATE_CURRENCY_GAME: string;
    static WILL_CLOSING_GAME_GENERATE_UNFINISH_GAME: string;
    static WINDOW_SET_TITLE: string;
  }
  class DefaultGmApi {
    initialized: boolean;
    publications: Publications;
    gameStates: {
      INITIALIZE: string;
      OPEN_PLAY: string;
      SETTLE_PLAY: string;
    };
    apiGameStates: {
      READY: string;
      GAME_IN_PROGRESS: string;
      SOFT_GAME_IN_PROGRESS: string;
      OVERLAY_SHOWN: string;
    };
    currentApiState: string;
    playId: string;
    guiBlocked: boolean;
    lobbyActive: boolean;
    musicActive: boolean;
    sfxActive: boolean;
    complianceMode: {
      UKGC: boolean;
    };
    gameOptions: {};
    private _callbacks;
    private _realityCheck;
    private _rcRemainingTime;
    private _shouldShowMessage;
    private _roundRequested;
    constructor(realityCheck: gcw.api.RealityCheck | null);
    setRealityCheckTimer(): void;
    showMessage(): void;
    finishSoftGameState(): void;
    publishEvent(event: any, data: any): void;
    goToLobby(): void;
    getActualPlayId(): any;
    setActualPlayId(playId: any): void;
    isGUIBlocked(): boolean;
    getLobbyActive(): boolean;
    getCurrencyFormatting(): Object;
    getMusicActive(): boolean;
    getSfxActive(): boolean;
    setCurrencyFormat(
      prefix: any,
      suffix: any,
      decimalPlaces: any,
      decimalSeparator: any,
      groupingSeparator: any,
      prefixSeparator: any,
      suffixSeparator: any
    ): void;
    sendSessionRequest(): void;
    subscribeToEvent(event: any, callback: (data: any) => void): string;
    unsubscribeFromEvent(subscriptionId: any): void;
    private _publishEvent;
  }
}
declare module gcw.properties {
  var version: string;
  var suppressObfuscation: boolean;
}
declare module gcw.obfuscation {
  class Translator {
    transcode(message: any[]): any[];
    stringToArray(str: string): any;
  }
}
declare module gcw.http {
  class HttpRequest {
    private _url;
    private _method;
    private _onError;
    private _onResponse;
    private _async;
    url(url: string): HttpRequest;
    method(method: string): HttpRequest;
    onError(onError: (err: any) => void): HttpRequest;
    onResponse(onResponse: (data: any) => void): HttpRequest;
    async(async: boolean): HttpRequest;
    send(data?: any): void;
    sendWithCallbacks(
      onResponse: (data: any) => void,
      onError: (err: any) => void,
      data?: any
    ): void;
    private isSpinEvent;
    private createRequestData;
    private getResponse;
    transcode(msg: any): any;
    arrayToString(array: any): string;
    stringToArray(str: string): any;
    private decodeUint8array;
  }
}
declare module gcw.ngs {
  interface NgsRequest {
    id?: number;
    date?: string;
    game?: any;
    operation?: any;
    configuration?: any;
    jackpot?: any;
  }
  interface NgsResponse {
    id?: number;
    status?: NgsResponseStatus;
    game?: any;
    operation?: any;
    configuration?: any;
    jackpot?: any;
  }
  interface NgsResponseStatus {
    code: string;
    message: string;
  }
  class NgsClient {
    private _isInitialized;
    private _url;
    private _heartBeat;
    private _requestInProgress;
    private _heartBeatTimeout;
    private _httpConnection;
    private _requestIdCounter;
    private _onConnectionResponse;
    private _onConnectionError;
    private _unsentRequest;
    private _responseErrorCounter;
    private isReplay;
    constructor(data: gcw.api.InitializeData);
    getUrl(): string;
    setUrl(url: string): NgsClient;
    getHeartBeat(): number;
    setHeartBeat(value: number): NgsClient;
    onResponse(onResponse: (response: NgsResponse) => void): NgsClient;
    onError(onError: (err: Error) => void): NgsClient;
    start(): NgsClient;
    stop(): NgsClient;
    send(request: NgsRequest): NgsClient;
    private _setHeartBeatTimeout;
    private _clearHeartBeatTimeout;
    private _onHeartBeat;
    private _sendSequentialRequest;
    private _onResponseErrorWithRepeat;
    private _onResponse;
    private _onResponseError;
    private _sendNonSequentialRequestAndForget;
    private _sendNonSequentialRequest;
    private _onNonSequentialResponse;
    private _ignoreResponse;
    private _onNonSequentialResponseError;
    private _onHeartbeatResponse;
  }
}
declare module gcw.load {
  function loadScriptPrivate(
    url: string,
    namespace: string,
    callback?: (lib: any) => void
  ): void;
  function loadScript(url: string, callback?: () => void): void;
  function loadScripts(urls: string[], callback?: () => void): void;
  function loadImage(url: string, callback?: () => void): void;
  function loadCss(url: string, callback?: () => void): void;
}
declare module gcw.adapter {
  class GcwError {
    private static _codes;
    static OK: {
      code: string;
    };
    static INTERNAL_SERVER_ERROR: {
      code: string;
    };
    static INVALID_REQUEST: {
      code: string;
    };
    static INVALID_REQUEST_ID: {
      code: string;
    };
    static INVALID_DEFENCE_CODE: {
      code: string;
    };
    static UNKNOWN_GAME_SESSION: {
      code: string;
    };
    static GAME_SESSION_CLOSED: {
      code: string;
    };
    static GAME_CLIENT_CLOSED: {
      code: string;
    };
    static GAME_CLIENT_OBSOLETE: {
      code: string;
    };
    static GAME_RESTRICTED_IN_COUNTRY: {
      code: string;
    };
    static CASINO_PLATFORM_INTEGRATION_ERROR: {
      code: string;
    };
    static REQUEST_IN_PROCESSING: {
      code: string;
    };
    static INVALID_STAKE_VALUES: {
      code: string;
    };
    static FUNDS_STRUCTURE_CHANGED: {
      code: string;
    };
    static INSUFFICIENT_FUNDS: {
      code: string;
    };
    static GAME_LIMITS_RECONFIGURED: {
      code: string;
    };
    static errors: {
      code: string;
    }[];
    private static _staticInit;
    static hasErrorCode(code: string): boolean;
    static getErrorByCode(code: string): GcwError;
    static isFatal(code: string): boolean;
  }
  class NgsGameAdapter implements gcw.api.Game {
    private _game;
    private _eventListener;
    private _ngsClient;
    private _postponeFreeRounds;
    private _loggingServiceUrl;
    constructor(game: gcw.api.Game, loggingServiceUrl: string);
    setEventListener(eventListener: (type: string, data?: any) => void): void;
    getGameLogRequestData(data: any): api.GameLogRequestData;
    sendEvent(type: string, data?: any): void;
    getNgsClientUrl(): string;
    private _eventListenerCall;
  }
}
declare module gcw.l12n {
  class L12n {
    static RESOURCE_BUNDLE: any;
  }
}
declare module gcw.adapter {
  interface CasinoErrorData {
    errorCode: string;
    errorMessage: string;
    data: any;
  }
  class CasinoGameAdapter implements gcw.api.Game {
    private _game;
    private _gameInitData;
    private _initConf;
    private _eventListener;
    private _gmApi;
    private _errorData;
    private _gameInitialized;
    private _freePlaysData;
    private _hasFreePlaysData;
    private _isInterrupted;
    private _loggingServiceUrl;
    constructor(
      game: gcw.adapter.NgsGameAdapter,
      initConf: gcw.api.InitConf,
      realityCheck: gcw.api.RealityCheck,
      integrationData: any,
      loggingServiceUrl: string
    );
    setEventListener(eventListener: (type: string, data?: any) => void): void;
    private handleRoundEndWithTimeout;
    private handleRoundEnd;
    static findMessage(lang: string, error: gcw.api.Error): string;
    static getErrorSeverity(errorCode: string): string;
    sendEvent(type: string, data?: any): void;
    private _eventListenerCall;
    private _initializeFreeRounds;
    private _sendInitializeToGame;
    private _sendFreeRoundsToGmApi;
    private _freeRoundsAccepted;
    private _freeRoundsRejected;
    private _checkResponseForFreePlaysData;
    private _checkResponseForTimeoutData;
    private _updateFreePlaysStatus;
    private _clearFreePlaysData;
    private _casinoSpecificInitialize;
  }
  class CasinoError {
    private static _codes;
    static FATAL_ERROR: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_STAKE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INCORRECT_VALUES_STAKE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static SERVICE_UNDER_MAINTENANCE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static UNABLE_TO_LOCK_SESSION: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_LICENSE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_GAME: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_CASINO_OPERATOR: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_GAME_MODE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_USER_IDENTIFIER: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INITIAL_BALANCE_REQUIRED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static EXPIRED_LICENSE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INVALID_CURRENCY: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static PLAY_NOT_READY_TO_SETTLE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static THE_SESSION_HAS_EXPIRED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static THE_SESSION_IS_FINALIZED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static CONNECTION_ERROR: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static FREEPLAY_NOT_ALLOWED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static INSUFFICIENT_FUNDS: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static SERVICE_TEMPORARILY_UNAVAILABLE: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static RESPONSIBLE_GAMING_SESSION_LIMIT_REACHED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static RESPONSIBLE_GAMING_LOSS_LIMIT_REACHED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static RESPONSIBLE_GAMING_BLOCKED_TEMPORARILY: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static MAXIMUM_WIN_REACHED: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static UNFINISHED_GAME_NOT_SHOWN: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static UNFINISHED_GAME: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static CUSTOM_MESSAGES: {
      code: number;
      msg: string;
      fatal: boolean;
    };
    static errors: {
      code: number;
      msg: string;
      fatal: boolean;
    }[];
    private static _staticInit;
    static hasErrorCode(code: number): boolean;
    static toCasinoError(error: any): any;
    static apiToCasinoError(error: any): any;
    static getErrorByCode(code: number): any;
  }
}
declare var minSpinDuration: any;
interface Window {
  attachEvent(event: string, listener: EventListener): boolean;
  detachEvent(event: string, listener: EventListener): void;
}
interface HTMLScriptElement {
  crossOrigin: string | null;
}
declare module gcw {
  function gameElementsInit(
    gameEventListener?: (type: string, data?: any) => void,
    callback?: (game: gcw.api.Game, err?: any) => void,
    integrationData?: any
  ): void;
  function gameElementInit(
    gameElement: HTMLElement,
    gameEventListener: (type: string, data?: any) => void,
    callback?: (game: gcw.api.Game, err?: any) => void,
    integrationData?: any
  ): void;
  function gameStart(
    gameContainer: HTMLElement,
    gameSessionUrl: string,
    gameEventListener: (type: string, data?: any) => void,
    gameLibUrl?: string,
    replayConf?: gcw.api.ReplayConf,
    callback?: (game: gcw.api.Game, err?: any) => void,
    integrationData?: any
  ): void;
  function loadGame(
    sessionUrl: string,
    callback: (
      gameLib: any,
      initConf: gcw.api.InitConf,
      realityCheck: gcw.api.RealityCheck,
      replayConf: gcw.api.ReplayConf,
      loggingServiceUrl: string,
      err?: any
    ) => void,
    gameLibUrl?: string,
    replayConf?: gcw.api.ReplayConf
  ): void;
  function handleError(
    evt: any,
    error: any,
    sessionUrl: string,
    loggingServiceUrl: string
  ): void;
  function handleGameLog(
    sessionUrl: string,
    loggingServiceUrl: string,
    data: gcw.api.GameLogRequestData
  ): void;
  function loadGameLib(
    gameLibUrl: string,
    callback: (gameLib: any) => void
  ): void;
}
declare var ngc: typeof gcw;
declare module gcw.protocol {
  interface JackpotTopic {}
}
declare module gcw.protocol {
  class StatusCode {
    static OK: string;
    static REQUEST_IN_PROCESSING: string;
    static FUNDS_STRUCTURE_CHANGED: string;
    static INSUFFICIENT_FUNDS: string;
    static INTERNAL_SERVER_ERROR: string;
    static INVALID_REQUEST: string;
    static INVALID_DEFENCE_CODE: string;
    static UNKNOWN_GAME_SESSION: string;
    static UNKNOWN_GAME_ROUND: string;
    static GAME_RESTRICTED_IN_COUNTRY: string;
    static GAME_SESSION_CLOSED: string;
    static GAME_CLIENT_CLOSED: string;
    static GAME_CLIENT_OBSOLETE: string;
    static CASINO_PLATFORM_INTEGRATION_ERROR: string;
    static INVALID_STAKE_VALUES: string;
  }
}
