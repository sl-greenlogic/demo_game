/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { SceneLayer } from '../sdk/core/controllers/sceneController';
import { GameSettings } from '../settings/game.settings';
import { SlotMachine } from '../sdk/core/slotmachine';
import { Scene } from '../sdk/core/basecomponents/scene';
import { ExternalSlotController } from '../ExternalSlotController';

export class GameScene extends Scene {
  private game!: any;
  private gameControls!: any;
  private fsmStarted!: boolean;

  constructor(layer: SceneLayer) {
    super(layer);
    this.fsmStarted = false;
    this.create();
  }

  protected create(): void {
    SlotMachine.init(() => this.onFsmStarted());
  }

  public update(delta: number): void {
    if (this.fsmStarted) this.game.update(delta);
  }

  public resize(): void {}

  private onFsmStarted(): void {
    this.fsmStarted = true;
    this.gameControls = new GameSettings.GameControlClass(this);
    this.game = new GameSettings.GameClass(this, this.gameControls);
    ExternalSlotController.getInstance().setGame(this.game);
    this.game.setupGameLayers();
    this.game.setupGameBackground();
    this.game.setupGameForeGround();
    this.gameControls.addControlsToScene();
    this.game.addControlsListeners(this.gameControls);
    this.game.setupGame();
  }
}
