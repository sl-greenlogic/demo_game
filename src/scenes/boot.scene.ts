/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { SceneLayer } from '../sdk/core/controllers/sceneController';
import { App } from '../app';
import { Scenes } from '../settings/game.settings';
import { Scene } from '../sdk/core/basecomponents/scene';

export class BootScene extends Scene {
  constructor(layer: SceneLayer) {
    super(layer);
    this.create();
  }
  protected create(): void {
    PIXI.loader.add(
      'assets',
      'https://cdn.dev02-gs-stakelogic.com/slsdk/luckygemsprototype/assets.json'
    );
    PIXI.loader.add(
      'loading_bar',
      'https://cdn.dev02-gs-stakelogic.com/slsdk/luckygemsprototype/assets/images/loading_bar.png'
    );

    PIXI.loader.load(() =>
      App.sceneController.loadScene(Scenes.PreLoaderScene, SceneLayer.UI)
    );
  }

  public update(delta: number): void {}

  public resize(): void {}
}
