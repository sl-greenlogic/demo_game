/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

export interface GameControl extends PIXI.Container {
  addControlsToScene(container: PIXI.Container): void;
}
