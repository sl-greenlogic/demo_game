/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { GameStates } from '../core/slotmachine';

export enum InteractionTrigger {
  PointerDown,
  PointerUp,
  PointerOut,
  PointerOver,
}

export enum ButtonState {
  Active,
  HoverOver,
  HoverOut,
  Click,
  Release,
  ReleaseOutside,
}

export type ButtonStateTexture = {
  state: ButtonState;
  gameState: GameStates[];
  texture: string;
};

export interface CallBack {
  trigger: InteractionTrigger;
  function: Function;
}

export abstract class Button extends PIXI.Sprite {
  protected callBackPointerDown!: CallBack;
  protected callBackPointerUp!: CallBack;
  protected callBackPointerOut!: CallBack;
  protected callBackPointerOver!: CallBack;
  private enabled!: boolean;

  constructor() {
    super();
  }

  public enable(): void {
    this.enabled = true;
  }
  public disable(): void {
    this.enabled = false;
  }

  get activated(): boolean {
    return this.enabled;
  }

  public abstract enableInteraction(): void;
  public abstract disableInteraction(): void;
}
