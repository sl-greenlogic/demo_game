/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { DebugStyles } from '../../settings/debugger.settings';

export enum DebugMessageType {
  Info,
  Warn,
  Error,
  Success,
  Loaded,
}

export class GameDebugger {
  constructor(private debuggerEnabled: boolean) {}

  public info(msg: string) {
    if (this.debuggerEnabled) {
      console.log(`%c [` + this.getTime() + '] ' + msg, DebugStyles.Info);
    }
  }
  public warn(msg: string) {
    if (this.debuggerEnabled) {
      console.log(`%c [` + this.getTime() + '] ' + msg, DebugStyles.Warn);
    }
  }

  public success(msg: string) {
    if (this.debuggerEnabled) {
      console.log(`%c [` + this.getTime() + '] ' + msg, DebugStyles.Success);
    }
  }
  public loaded(msg: string) {
    if (this.debuggerEnabled) {
      console.log(`%c [` + this.getTime() + '] ' + msg, DebugStyles.Loaded);
    }
  }

  private getTime(): string {
    var date = new Date();
    var hour = date.getHours();
    var minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    var seconds = (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
    return `${hour}:${minutes}:${seconds}`;
  }
}
