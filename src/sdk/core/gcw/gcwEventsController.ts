/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { App } from '../../../app';
import { ExternalSlotController } from '../../../ExternalSlotController';

export type GcwEventListenerHashMap = {
  [id: number]: GcwEventListener;
};
export type GcwEventListener = {
  type: gcw.api.GameEventType;
  callback: Function;
  singleCall: boolean;
};

export class GcwEventsController {
  private static gcwEventEmitters: Function[] = [];
  private static gcwEventListeners: GcwEventListenerHashMap = [];
  private static lastListenerKey: number = 0;
  private static ngsRequestId = 0;

  public static getGameClient(): void {
    var gameWrapperRoot: any = gcw;
    var gameWrapperGames =
      gameWrapperRoot.games || (gameWrapperRoot.games = {});
    return gameWrapperGames.gamename;
  }

  public static setEventEmitter(gcwEventEmitter: any) {
    this.gcwEventEmitters.push(gcwEventEmitter);
  }

  public static emitGcwEvent(type: gcw.api.GameEventType, data?: any) {
    // console.error('emitGcwEvent', type, data);
    if (type === gcw.api.GameEventType.NGS_REQUEST) {
      this.ngsRequestId++;
      data.id = this.ngsRequestId;
    }

    this.gcwEventEmitters.forEach((emitter: Function) => emitter(type, data));
  }
  public static receiveGcwEvent(type: gcw.api.GameEventType, data?: any) {
    console.error('receiveGcwEvent', type, data);
    App.debugger.info(`Gcw${type}:${data}`);

    if (type === 'NGS_RESPONSE') {
      ExternalSlotController.getInstance().updateModelFromMessage(data);
    }

    const listenersInterested = Object.keys(this.gcwEventListeners).filter(
      (key: any) => {
        return this.gcwEventListeners[key].type === type;
      }
    );

    listenersInterested.forEach((key: any) => {
      this.gcwEventListeners[key].callback(data);
      if (this.gcwEventListeners[key].singleCall) {
        delete this.gcwEventListeners[key];
      }
    });
  }
  public static subscribeForGcwEvent(
    type: gcw.api.GameEventType,
    callBack: Function,
    singleCall: boolean = true
  ): number {
    // console.error('subscribeForGcwEvent', type);
    this.lastListenerKey++;
    this.gcwEventListeners[this.lastListenerKey] = {
      type: type,
      callback: callBack,
      singleCall: singleCall,
    };
    return this.lastListenerKey;
  }
  public static unSubscribeForGcwEvent(id: number) {
    // console.error('unSubscribeForGcwEvent', id);
    delete this.gcwEventListeners[id];
  }
}
