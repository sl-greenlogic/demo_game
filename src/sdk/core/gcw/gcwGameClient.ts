import { GcwEventsController } from './gcwEventsController';
import { App } from '../../../app';

export class GcwGameClient implements gcw.api.Game {
  setEventListener(eventListener: (type: string, data?: any) => void): void {
    if (!App.initialized) {
      App.initialized = true;
      GcwEventsController.setEventEmitter(eventListener);
    }
  }

  sendEvent(type: string, data?: any): void {
    GcwEventsController.receiveGcwEvent(type, data);
  }
}
