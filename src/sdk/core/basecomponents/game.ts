/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { GameControl } from '../../ui/gamecontrol';
import { GameStates } from '../slotmachine';

export interface Game {
  setupGameLayers(): void;
  setupGameBackground(): void;
  setupGameForeGround(): void;
  addControlsListeners(controls: GameControl): void;
  addGameListeners(): void;
  subscribeForFsmEvents(): void;
  setupGame(): void;
  update(delta: number): void;
  onFsmStateChanged(state: GameStates): void;
}
