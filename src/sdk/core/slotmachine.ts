/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { App } from '../../app';
import { GcwEventsController } from './gcw/gcwEventsController';
import { Howler } from 'howler';

const StateMachine = require('javascript-state-machine');

export enum GameStates {
  Ready = 'ready',
  Menu = 'menu',
  InitRound = 'startsround',
  PerformRound = 'round',
  PreStopRound = 'stoppinground',
  Stop = 'stop',
  Any = '',
}

export type StateOfMachine = {
  name: string;
  from: string;
  to: string;
};

export type FsmChangeStateListener = {
  state: GameStates;
  callback: Function;
  active: boolean;
};

// export type FsmCallback = {
//     state: GameStates,
//     callBackName: string,
//     callback: Function
// }

export class SlotMachine {
  private static instance: SlotMachine;
  private fsmStatusChangeListeners!: FsmChangeStateListener[];
  private fsm: any;

  /**
   * Create the SlotMachine Singlenton
   *
   * @static
   * @param {Scene} mainScene
   * @memberof SlotMachine
   */
  public static init(onStart: Function): void {
    if (!this.instance) {
      App.debugger.info(`Inform Initialization performed to gcw.`);
      GcwEventsController.subscribeForGcwEvent(
        gcw.api.GameEventType.START,
        () => {
          App.debugger.success(`Gcw authorized the start.`);
          this.instance = new SlotMachine();
          this.instance.fsmStatusChangeListeners = [];
          this.instance.startFsm();
          onStart();
          GcwEventsController.emitGcwEvent(gcw.api.GameEventType.START_SUCCESS);
        }
      );
      GcwEventsController.emitGcwEvent(
        gcw.api.GameEventType.INITIALIZE_SUCCESS
      );

      // TODO: fix this part,
      // because it should respond to START event
      App.debugger.success(`Gcw authorized the start.`);
      this.instance = new SlotMachine();
      this.instance.fsmStatusChangeListeners = [];
      this.instance.startFsm();
      onStart();
      GcwEventsController.emitGcwEvent(gcw.api.GameEventType.START_SUCCESS);
      Howler.mute(true);
    }
  }

  private startFsm(): void {
    // Setup the State of Machine
    this.fsm = new StateMachine({
      init: 'ready',
      transitions: [
        {
          name: 'ready',
          from: ['ready', 'stoppinground', 'menu'],
          to: 'ready',
        },
        {
          name: 'menu',
          from: 'ready',
          to: 'menu',
        },
        {
          name: 'initround',
          from: 'ready',
          to: 'startsround',
        },
        {
          name: 'performround',
          from: 'startsround',
          to: 'round',
        },
        {
          name: 'prestopround',
          from: 'round',
          to: 'stoppinground',
        },
        {
          name: 'stopround',
          from: 'stoppinground',
          to: 'ready',
        },
      ],
    });

    // Start StateMachine
    this.fsm.ready();
    this.emitFsmStatusChanged(GameStates.Ready);
  }

  public static subscribeForFsmStatusChange(
    listener: FsmChangeStateListener
  ): number {
    const newIndex = this.instance.fsmStatusChangeListeners.length + 1;
    this.instance.fsmStatusChangeListeners[newIndex] = listener;

    return newIndex;
  }
  public static unSubscribeForFsmStatusChange(id: number) {
    this.instance.fsmStatusChangeListeners[id].active = true;
  }

  public static get fsmState(): GameStates {
    return this.instance.fsm.state;
  }

  public static changeState(state: GameStates) {
    switch (state) {
      case GameStates.Menu:
        this.instance.fsm.menu();
        break;
      case GameStates.InitRound:
        this.instance.fsm.initround();
        break;
      case GameStates.PerformRound:
        this.instance.fsm.performround();
        break;
      case GameStates.PreStopRound:
        this.instance.fsm.prestopround();
        break;
      case GameStates.Stop:
        this.instance.fsm.stopround();
        break;
      case GameStates.Ready:
        this.instance.fsm.ready();
        break;
    }

    this.instance.emitFsmStatusChanged(state);
  }

  private emitFsmStatusChanged(state: GameStates): void {
    const actives = this.fsmStatusChangeListeners.filter((listener) => {
      return listener.active === true;
    });
    actives.forEach((listener) => {
      if (listener.state === state || listener.state === GameStates.Any) {
        listener.callback(state);
      }
    });
  }
}
