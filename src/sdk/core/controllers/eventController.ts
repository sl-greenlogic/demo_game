// /*!
// * @author       StakeLogic <info@stakelogic.com>
// * @copyright    2019 Stakelogic
// * @description  19SDK
// * @license      Stakelogic
// */

// import { GameEvent } from "../basecomponents/event";
// import { App } from "../../../app";
// import { ControlEvents, GameEvents } from "../../../settings/game.settings";

// export type EventType = {
//     receiver : any,
//     callBack : Function
// }

// export class EventController {

//     private events: { [key: string]: EventType } = {};
//     private availableEvents! : string[];

//     constructor() {
//         this.availableEvents = [];
//         this.availableEvents = this.availableEvents.concat(Object.values(ControlEvents));
//         this.availableEvents = this.availableEvents.concat(Object.values(GameEvents));
//     }

//     addEvent(name: string, callBack: Function, receiver? :any): void {
//         if (!this.availableEvents.includes(name)) {
//            throw ('Event not registered in game settings!');
//         }
//         if (this.events[name] && this.events[name].receiver === receiver) {

//             this.events[name] = { receiver : receiver, callBack : callBack}
//             if(receiver)
//             {
//                 receiver.on(this.events[name], callBack);
//             }
//             App.debugger.success(`Event ${name} updated successfully!`);
//         }
//         else {

//             this.events[name] = { receiver : receiver, callBack : callBack}
//             if(receiver)
//             {
//                 receiver.on(this.events[name], callBack);
//             }
//             App.debugger.success(`Event ${name} registered successfully!`);
//         }
//     }

//     removeEvent(name: string): void {
//         delete this.events[name];
//     }

//     emit(name: string, args?: any) {
//         return new Promise((resolve) => {
//             if (args)
//                 this.events[name].callBack(args);
//             else
//                 this.events[name].callBack();
//             resolve();
//         }).then(
//             () => {
//                 App.debugger.success(`Event ${name} triggered successfully!`)
//             }
//         ).catch(
//             () => {
//                 throw(`Event ${name} does not exist!`)
//             }
//         )
//     };
// }
