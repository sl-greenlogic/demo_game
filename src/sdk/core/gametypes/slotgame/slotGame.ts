/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { Game } from '../../basecomponents/game';

export interface SlotGame extends Game {
  setupReelsSet(amount: number): void;
  startSpin(): void;
  onInitSpin(): void;
  onSpin(): void;
  onPreStopSpin(): void;
}
