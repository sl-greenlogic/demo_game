/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { Container } from 'pixi.js';
import { ReelsStrips } from '../../../../settings/slotmachine.settings';

export enum ReelStatus {
  StartingSpinning,
  PreSpinCompleted,
  Spinning,
  StoppingSpinning,
  ReadyToStart,
}

export type ReelResult = {
  [reelId: number]: number[];
};

export abstract class Reel extends Container {
  protected reelId!: number;
  public status!: ReelStatus;
  protected stripInUse!: number[];
  protected positionInReelSet!: number;
  abstract setupReel(): void;
  abstract preStartSpin(): void;
  abstract startSpin(): void;
  abstract preStopSpin(stopPositions: number[], delay: number): void;
  abstract stopSpin(difference: number): void;
  abstract update(delta: number): void;

  constructor() {
    super();
    this.status = ReelStatus.ReadyToStart;
  }

  protected resetInternalStrip(): void {
    this.stripInUse = [];
    this.stripInUse = this.stripInUse.concat(
      ReelsStrips[this.positionInReelSet]
    );
  }
}
