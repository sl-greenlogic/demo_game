/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { ReelResult } from './reel';

export type ReelSetResult = {
  [reelSetId: number]: { reelId: number; result: number[] };
};
export enum ReelSetStatus {
  PerformingPreStart,
  PreStartCompleted,
  Spinning,
  ReadyToStart,
}

export abstract class ReelSet extends PIXI.Container {
  public status!: ReelSetStatus;
  abstract setupReelSetLayers(): void;
  abstract setupReelSetBackground(): void;
  abstract setupReelSetForeground(): void;
  abstract addGameListeners(): void;
  abstract setupReels(): void;
  abstract preStartSpin(): void;
  abstract startSpin(): void;
  abstract preStopSpin(result: ReelResult, delay: number): void;
  abstract stopSpin(): void;
  abstract update(delta: number): void;

  constructor() {
    super();
    this.status = ReelSetStatus.ReadyToStart;
  }
}
