/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { Sprite } from 'pixi.js';

export abstract class ReelSymbol extends Sprite {
  protected symbolId!: number;

  constructor() {
    super();
  }
}
