/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

export class Utils {
  static checkOverlap(a: any, b: any): boolean {
    var ab = a.getBounds();
    var bb = b.getBounds();
    return (
      ab.x + ab.width > bb.x &&
      ab.x < bb.x + bb.width &&
      ab.y + ab.height > bb.y &&
      ab.y < bb.y + bb.height
    );
  }

  static removeArrayItem(item: any, arr: any, allelements = true): void {
    for (var i = arr.length - 1; i >= 0; i--) {
      if (arr[i] === item) {
        arr.splice(i, 1);
        if (!allelements) break;
      }
    }
  }

  static getDistance(posA: PIXI.Point, posB: PIXI.Point): number {
    return Math.sqrt(
      Math.pow(posA.x - posB.x, 2) + Math.pow(posA.y - posB.y, 2)
    );
  }

  static generateRandomNumber(
    min: number,
    max: number,
    integer = false
  ): number {
    max += 1;
    var number = Math.random() * (max - min) + min;
    if (integer) number = Math.floor(number);

    return number;
  }

  static randomizeList<T>(array: T[]): T[] {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }
}
