export class WebUtils {
  public static isPortrait(): boolean {
    return window.innerHeight > window.innerWidth;
  }
  public static isMobileLandscape(): boolean {
    return WebUtils.isMobile() && !WebUtils.isPortrait();
  }

  public static isMobile(): boolean {
    return (
      /Mobi/i.test(navigator.userAgent) ||
      /Android/i.test(navigator.userAgent) ||
      /iPhone/i.test(navigator.userAgent) ||
      /iPad/i.test(navigator.userAgent) ||
      /iPod/i.test(navigator.userAgent) ||
      /webOS/i.test(navigator.userAgent) ||
      /BlackBerry/i.test(navigator.userAgent) ||
      /Windows Phone/i.test(navigator.userAgent) ||
      /Opera Mini/i.test(navigator.userAgent) ||
      /IEMobile/i.test(navigator.userAgent) ||
      /WPDesktop/i.test(navigator.userAgent)
    );
  }
}
