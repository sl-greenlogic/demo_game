/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { App } from './app';
import { GcwEventsController } from './sdk/core/gcw/gcwEventsController';
import { GcwGameClient } from './sdk/core/gcw/gcwGameClient';
import { ExternalSlotController } from './ExternalSlotController';

// App.application;

const gameWrapperRoot = gcw as any;
const gameWrapperGames = gameWrapperRoot.games || (gameWrapperRoot.games = {});
gameWrapperGames.gamename = { base: null, Game: GcwGameClient };

GcwEventsController.subscribeForGcwEvent(
  gcw.api.GameEventType.INITIALIZE,
  (data: any) => {
    App.init();
    GcwEventsController.emitGcwEvent(
      gcw.api.GameEventType.TOTAL_BET_UPDATE,
      ExternalSlotController.getInstance().getParameter('bet') *
        ExternalSlotController.getInstance().getParameter('totalBetMultiplier')
    );

    // console.error(data);

    // Update model with initial config
    ExternalSlotController.getInstance().initConf = data.initConf;

    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.NGS_REQUEST, {
      id: 0,
      date: new Date(),
      operation: { init: true, exit: false },
      game: {
        buttonCode: null,
        confirm: null,
        input: null,
        hearthBeatForceGetBalance: null,
        init: true,
        forfeitFreeRounds: null,
        forfeitBonus: null,
        payInAmount: null,
      },
      configuration: { init: true },
      jackpot: { init: true, timestamp: Date.now() },
      social: { init: true },
    });

    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.NGS_REQUEST, {
      id: 0,
      date: new Date(),
      configuration: {
        init: false,
        save: {
          general: { sounds: true },
          game: {
            intro_screen: true,
            animations: true,
            balanceInCurrency: true,
            balanceInCoins: false,
            bet_max: false,
            line_bet: 1,
            coin_value: 1,
            fast_spin: false,
            spacebar_spin: true,
            game_sounds: true,
            background_sounds: true,
            play_twin: false,
            slideable_touch: false,
            touch_anywhere: false,
          },
        },
      },
    });
  }
);
