// type Common = Record<string, {enabled: boolean, visible: boolean}>;
import { IElement } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/IElement';

interface StateConfig {
  INITIALIZE: Partial<IElement>;
  IDLE: Partial<IElement>;
  SPINNING: Partial<IElement>;
}

export const stateConfig: StateConfig = {
  INITIALIZE: {
    BUTTON_SPIN_STOP: { enabled: false, visible: false },
    BUTTON_SPIN: { enabled: true, visible: true },
    BUTTON_MENU: { enabled: false, visible: true },
    BUTTON_BET_SETTINGS: { enabled: false, visible: true },
    BUTTON_BET_PLUS: { enabled: false, visible: true },
    BUTTON_BET_MINUS: { enabled: false, visible: true },
    BUTTON_FAST_SPIN: { enabled: false, visible: true },

    BUTTON_SET_AUTO_PLAY_OPTION: { enabled: false, visible: true },
    BUTTON_BET_OPTION: { enabled: false, visible: true },
    BUTTON_MAX_BET: { enabled: false, visible: true },
    BUTTON_AUTO_SPIN: { enabled: false, visible: true },

    BUTTON_SOUNDS: { enabled: false, visible: true },
    BUTTON_SUPERSTAKE: { enabled: false, visible: true },
  },
  IDLE: {
    BUTTON_SPIN_STOP: { enabled: false, visible: false },
    BUTTON_SPIN: { enabled: true, visible: true },
    BUTTON_MENU: { enabled: true, visible: true },
    BUTTON_BET_SETTINGS: { enabled: true, visible: true },
    BUTTON_BET_PLUS: { enabled: true, visible: true },
    BUTTON_BET_MINUS: { enabled: true, visible: true },
    BUTTON_FAST_SPIN: { enabled: true, visible: true },

    BUTTON_SET_AUTO_PLAY_OPTION: { enabled: true, visible: true },
    BUTTON_BET_OPTION: { enabled: true, visible: true },
    BUTTON_MAX_BET: { enabled: true, visible: true },
    BUTTON_AUTO_SPIN: { enabled: true, visible: true },

    BUTTON_SOUNDS: { enabled: true, visible: true },
    BUTTON_SUPERSTAKE: { enabled: true, visible: true },
  },
  SPINNING: {
    BUTTON_SPIN: { enabled: false, visible: false },
    BUTTON_SPIN_STOP: { enabled: true, visible: true },
    BUTTON_MENU: { enabled: false, visible: true },
    BUTTON_BET_SETTINGS: { enabled: false, visible: true },
    BUTTON_BET_PLUS: { enabled: false, visible: true },
    BUTTON_BET_MINUS: { enabled: false, visible: true },
    BUTTON_FAST_SPIN: { enabled: false, visible: true },

    BUTTON_SET_AUTO_PLAY_OPTION: { enabled: false, visible: true },
    BUTTON_BET_OPTION: { enabled: false, visible: true },
    BUTTON_MAX_BET: { enabled: false, visible: true },
    BUTTON_AUTO_SPIN: { enabled: false, visible: true },

    BUTTON_SOUNDS: { enabled: true, visible: true },
    BUTTON_SUPERSTAKE: { enabled: false, visible: true },
  },
};
