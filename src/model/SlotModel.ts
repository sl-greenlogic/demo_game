import { IGeneral } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/IGeneral';
import { AdditionSectionType } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/AdditionSectionType';
import { LeaderboardType } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/LeaderboardType';
import { LayoutName } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/LayoutName';
// import {WrapperPopupType} from "@stakelogic_greenlogic/enriched_wrapper_types/interfaces/WrapperPopupType";
// import {WrapperPopupActionType} from "@stakelogic_greenlogic/enriched_wrapper_types/interfaces/WrapperPopupActionType";
// import {IRegulation} from "@stakelogic_greenlogic/enriched_wrapper_types/interfaces/IRegulation";
// import {GcwEventsController} from '../sdk/core/gcw/gcwEventsController';
import { GameType } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/GameType';
// import {ILeaderboardConfig} from "@stakelogic_greenlogic/enriched_wrapper_types";
// import {IAdditionSectionConfig} from "@stakelogic_greenlogic/enriched_wrapper_types/interfaces/IAdditionSectionConfig";

// import {IIntegration} from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/IIntegration';

export interface IModel {
  state: Partial<IGeneral>;
}

function getJsonFromUrl(url: string): any {
  const query: string = url.substr(1);
  let result: any = {};
  query.split('&').forEach(function (part) {
    var item = part.split('=');
    result[item[0]] = decodeURIComponent(item[1]);
  });

  return result;
}

function parseByBuild(path: string): string {
  let result = path.split('build')[0];

  return result;
}

class SlotModel implements IModel {
  currentURL: string = './';
  public static additionalConfig: any = {
    config: [
      {
        id: 522,
        name: 'LeaderBoardName',
        type: LeaderboardType.DAILY,
        value: 0.345,
        // "value": 2590.0778,
        startedAt: '2021-01-28T00:00:00Z',
        endedAt: '2021-10-19T00:00:00Z',
        // "endedAt": "2021-08-26T00:00:00Z",
        nrOfWinners: 7,
        distribution: [
          30, 20, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
          5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
        ],
        contributionPercentage: 0.35,
        instanceId: 1,
        reservePercentage: 0.35,
        seed: 25000,
        validFrom: '2021-10-11',
        validTo: '2021-10-18',
        playerPosition: {
          position: null,
          value: 0,
          win: 45,
          username: '',
        },
        topPositions: [
          {
            position: 1,
            value: 336.5,
            win: 5683.393,
            username: 'RGS***',
          },
          {
            position: 2,
            value: 13000.0,
            win: 152.67,
            username: 'RGS***',
          },
          {
            position: 3,
            value: 182.0,
            win: 65.67,
            username: 'RGS***',
          },
          {
            position: 4,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 5,
            value: 255.965,
            win: 255.965,
            username: 'RGS***',
          },
          {
            position: 6,
            value: 19.1974,
            win: 19.1974,
            username: 'RGS***',
          },
          {
            position: 7,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          //  {
          //    "position": 8,
          //    "value": 182.0,
          //    "win": 65.43000000,
          //    "username": "RGS***"
          //  },
          //  {
          //    "position": 9,
          //    "value": 182.0,
          //    "win": 65.43000000,
          //    "username": "RGS***"
          //  },
          //  {
          //    "position": 10,
          //    "value": 182.0,
          //    "win": 65.43000000,
          //    "username": "RGS***"
          //  },
          //  {
          //    "position": 11,
          //    "value": 182.0,
          //    "win": 65.43000000,
          //    "username": "RGS***"
          //  }
        ],
        previousTopPositions: [
          {
            position: 1,
            value: 336.5,
            win: 218.11,
            // "win": 0,
            username: 'RGS***',
          },
          {
            position: 2,
            value: 218.0,
            win: 152.67,
            username: 'RGS***',
          },
          {
            position: 3,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 4,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 5,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 6,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 7,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 8,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 9,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 10,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 11,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 12,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
        ],
      },
      {
        id: 523,
        name: 'LeaderBoardName',
        type: LeaderboardType.WEEKLY,
        value: 436.2,
        startedAt: '2021-01-29T00:00:00Z',
        // "endedAt": "2021-08-30T00:00:00Z",
        endedAt: '2021-10-25T00:00:00Z',
        nrOfWinners: 11,
        distribution: [
          30, 20, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
          5, 5, 5, 5,
        ],
        contributionPercentage: 0.35,
        playerPosition: {
          position: 2,
          value: 0,
          win: 218.1,
          username: '',
        },
        topPositions: [
          {
            position: 1,
            value: 336.5,
            win: 218.1,
            // "win": 0,
            username: 'RGS***',
          },
          {
            position: 2,
            value: 218.0,
            win: 152.67,
            username: 'RGS***',
          },
          {
            position: 3,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 4,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 5,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 6,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          {
            position: 7,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
          // {
          //   "position": 8,
          //   "value": 182.0,
          //   "win": 65.43000000,
          //   "username": "RGS***"
          // },
          // {
          //   "position": 9,
          //   "value": 182.0,
          //   "win": 65.43000000,
          //   "username": "RGS***"
          // },
          // {
          //   "position": 10,
          //   "value": 182.0,
          //   "win": 65.43000000,
          //   "username": "RGS***"
          // },
          // {
          //   "position": 11,
          //   "value": 182.0,
          //   "win": 65.43000000,
          //   "username": "RGS***"
          // }
        ],
        previousTopPositions: [
          {
            position: 1,
            value: 336.5,
            win: 218.1,
            username: 'RGS***',
          },
          {
            position: 2,
            value: 218.0,
            win: 152.67,
            username: 'RGS***',
          },
          {
            position: 3,
            value: 182.0,
            win: 65.43,
            username: 'RGS***',
          },
        ],
      },
    ],
    // config: [],
    type: AdditionSectionType.Leaderboard,
  };

  public state: Partial<IGeneral>;
  public assets: any;

  constructor() {
    const parsedParams = getJsonFromUrl(
      window.parent.location.search.toString()
    );

    if (parsedParams.gameJs) {
      this.currentURL = parseByBuild(parsedParams.gameJs);
    }

    // console.error(this.currentURL, parsedParams);

    this.state = {
      // integration: IIntegration.PokerStar,
      gameType: GameType.Slot,
      isRequestFullScreen: true,
      // totalWinFreeRoundsValue: 10,
      // totalWinFreeSpinValue: 20,
      // freespinsValue: '1/4',
      // freeroundsValue: 23,
      // multiplierValue: 10,
      // replayConfig: {
      //   sessionId: '123123',
      //   gameRoundId: '123123',
      //   date: '20/10/2020',
      //   time: '10:30',
      //   previous: () => {
      //     console.log("prev action");
      //   },
      //   next: () => {
      //     console.log("next action");
      //   },
      //   reload: () =>  {
      //     console.log("reload action");
      //     GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      //       general: {
      //         replayConfig: {
      //           state: 'start',
      //         }
      //       }
      //     });
      //   },
      //   stop: () => {
      //     console.log("stop action");
      //     GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      //       general: {
      //         replayConfig: {
      //           state: 'reload',
      //         }
      //       }
      //     });
      //   },
      //   start: () => {
      //     console.log("start action");
      //     GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      //       general: {
      //         replayConfig: {
      //           state: 'stop',
      //         }
      //       }
      //     });
      //   },
      //   state: 'start',
      // },
      // customSettingSections: [
      //   {
      //     title: 'hold',
      //     subSections: [
      //       {
      //         title: 'Hold spin',
      //         eventName: 'BUTTON_TOGGLE_HOLD',
      //         isActive: false,
      //         isEnabled: true,
      //         isVisible: true,
      //       }
      //     ]
      //   }
      // ],
      // additionSectionConfig: {
      //   config:{
      //     isDemo: false,
      //     currencySymbol: '$',
      //     "miniJackpotAmount":726.42,
      //     "miniJackpotPercent":0,
      //     "grandJackpotAmount":1600000,
      //     "grandJackpotPercent":0,
      //     miniJackpotMinimalAmount: 1000,
      //     miniJackpotMaximumAmount: 5000,
      //     grandJackpotMinimalAmount: 2222,
      //     grandJackpotMaximumAmount: 2222,
      //     // "logo":"https://cdn.dev02-gs-stakelogic.com/libs/stakelogic/ew-0.1.18.3/img/buttons/bttn_MENU_SUPER_push.png",
      //     "miniJackpotWinners":[
      //       {
      //         date: '17/02/21',
      //         time: '13:28',
      //         amount: 5000,
      //       }
      //     ]},
      //   type : AdditionSectionType.GrandJackpot
      // },

      additionSectionConfig: {
        ...SlotModel.additionalConfig,
      },
      copyrights:
        'Super Stake side-bet triggers base game features and will not be shown in free spains',
      isGameLocked: false,
      currency: '€',
      currencyDelimiter: 2,
      balance: 1000,

      bet: 1,
      betRange: [
        1, 2, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 70000, 800000,
        90000000,
      ],

      isSuperstakeEnabled: false,

      isFastSpinEnabled: false,

      gameMessage: null,

      isAutoplayEnabled: false,
      autoplayCount: 100,
      isAutoplayStopOnBonus: false,
      autoplayRange: [5, 10, 25, 50, 100, 200, 500, 1000, 1500, 2000, 2500, -1],
      autoplayLossLimit: 0,
      autoplayLossLimitRange: [0, 2, 3, 4, 5, 6, 7],
      autoplaySingleWinLimit: 0,
      autoplaySingleWinLimitRange: [0, 20000, 300000, 4000000, 50000000, 6, 7],
      autospinsLeft: '',

      hasIntroScreen: true,
      hasGamble: true,
      isSpacebarSpin: true,
      isTouchAnywhereSpin: false,
      isSlidableTouch: false,
      // isMuted: true,
      isSoundEnabled: false,
      isMusicEnabled: false,

      useWrapperAudio: true,

      useLayoutSet: 0,

      regulation: {
        isDisplaySessionTime: false,
        isDisplayNet: false,
        UKGC: true,
        inactivityMessage: true,
      },

      popups: [
        // {
        //   type: WrapperPopupType.Info,
        //   onClose: null,
        //   title: 'Demo',
        //   text: 'Some dialog text',
        //   actions: [
        //     {
        //       type: WrapperPopupActionType.Ok,
        //       text: 'OK',
        //       action: () => {
        //       },
        //     },
        //     {
        //       type: WrapperPopupActionType.Cancel,
        //       text: 'CANCEL',
        //       action: () => {
        //       },
        //     }
        //   ]
        // }
      ],

      gameName: 'Lucky Gems',
      gameVersion: 'v.1.0.13',

      clock: '',
      // grandJackpotConfig: {
      //   maxConfig: {
      //     currency: 'EUR',
      //     amount: 5000,
      //   },
      //   miniJackpotAmount: 4950,
      //   miniJackpotPercent: 40,
      //   grandJackpotAmount: 800000,
      //   grandJackpotPercent: 70,
      //   logo: null,
      //   list: [
      //     {date: '17/02/21', time: '19:13', currency: '$', amount: 5000},
      //     {date: '17/02/21', time: '19:13', currency: '$', amount: 5000},
      //     {date: '17/02/21', time: '19:13', currency: '$', amount: 5000},
      //   ]
      //
      // },
      // showGrandJackpot: false,
      serverName: 'Server ver.',
      serverVersion: '0.10.1',

      rtp: '96,5%',
      maxWin: 1000000,
      maxWinProbability: '1:45',

      rules: `
        <div class="rowBlock">
          <h3 class="rowTextBlock">
            Lucky Gems™ GAME RULES
          </h3>
        </div>
        <div class="rowBlock">
          <div class="rowTextBlock">
            Try your luck playing one game, two games, three games, or even four games at the same time with Lucky Gems Deluxe! All of the reels are filled with valuable gemstones and leprechauns that can help bring you more luck. Jolly leprechaun symbols can fill an entire reel. Any giant leprechaun symbol, or part(s) of a giant leprechaun symbol, visible on the reels transform into the same matching regular symbol. The matching symbols that the leprechaun awards are mystery symbols, so you could get extra lucky and get awarded with the most valuable gem stone symbols! Is today your lucky day to win the pot of gems at the end of the rainbow? Play Lucky Gems Deluxe for your chance at some big wins, with a touch of added luck!
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            Try your chances at a BIG WIN or a MEGA WIN in this hot new video slot game featuring 10 win lines, 5 reels, and 3 rows per game in each of the four (4) possible games. The colorful reels are filled with Lucky Gems Deluxe, Lucky Gems Deluxe game symbols, and stacked leprechaun mystery symbols. Choose to play between one (1) and four (4) different games at the same time, complete with synchronized spinning action.
          </div>
        </div>
        
        <div class="rowBlock">
          <div class="rowTextBlock">
            There are four (4) different game options to choose from that allow you to play one (1), two (2), three (3), or four (4) different games at the same time with the same stake. The four (4) different sets of game reels feature synchronized spins and reel stops.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            Game features include STACKED MYSTERY SYMBOLS, which display a LEPRECHAUN character in three (3) different parts, which cover an entire reel if the entire STACKED MYSTERY SYMBOL is visible. If any part of a LEPRECHAUN STACKED MYSTERY SYMBOL lands on the reels, and if more than one STACKED MYSTERY SYMBOL lands on the reels on the same spin, all of the visible LEPRECHAUN SYMBOLS will transform into the same matching regular symbol from the reels. One (1) regular symbol is randomly selected on each winning spin to be the mystery replacement symbol.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            Click/Tap the SPIN or AUTO PLAY BUTTONS to start playing. Activate MAX BET to play with the maximum allowable TOTAL BET settings. Use the – and + BUTTONS to customize your bet settings. You can also use these buttons to choose to play GAME 1, GAME 1-2, GAME 1-3, or GAME 1-4. You can also use the SELECT GAME numbers (1, 2, 3, or 4) to choose how many games you want to play at the same time.
          </div>
        </div>
        
        <div class="rowBlock">
          <div class="rowTextBlock">
            The theoretical minimum payback percentage for this game is 96.07%.
          </div>
        </div>
        
        <div class="rowBlock decor">
          <div class="rowTextBlock">
            Malfunction voids all pays and plays.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_MENU_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>GENERAL SETTINGS</h3>
            Click/tap GENERAL SETTINGS button to open the general settings menu.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_SOUND_OFF_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>MUTE/UNMUTE</h3>
            Click/tap the MUTE/UNMUTE button to mute/unmute all game sounds.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_CHOOSE_BET_COIN_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>BET SETTINGS</h3>
            Click/tap the BET SETTINGS button to open the bet settings menu.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_FAST_SPIN_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>TURBO SPIN</h3>
            Click/tap the TURBO SPIN button to active TURBO SPIN. The default setting for TURBO SPIN is OFF. When set to ON, the reels spin faster than normal spins.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_SPIN_regular.png" width="90px" />
          </div>
          <div class="rowTextBlock">
            <h3>SPIN</h3>
            Click the SPIN BUTTON or press SPACE BAR to start playing using the current TOTAL BET.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_AUTOSPIN_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>AUTO SPIN SETTINGS</h3>
            Click/tap the AUTO SPIN button to open the auto spin -enu.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_MIN_regular.png" />
            <img src="${this.currentURL}assets/rules/bttn_PLUS_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>TOTAL BET</h3>
            Use the - and + BUTTONS to customize your bet settings.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_menuClose_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>CLOSE</h3>
            Click/tap CLOSE to return to the game screen.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_CHOOSE_BET_COIN_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>BET SETTINGS</h3>
            <h3>MAX BET</h3>
            Click MAX BET to play with the maximum allowable TOTAL BET. Use the - and + BUTTONS to customize your bet settings.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_SUPER_STAKE2_active_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>SUPER STAKE</h3>
            Double your bet to activate a special SUPER STAKE mode for extra chances of hitting insanely valueble winning combinations!
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_settings_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>GENERAL SETTINGS</h3>
            <h3>MAIN</h3>
            <h3>INTRO SCREEN</h3>
            The default setting for the INTRO SCREEN (if available) is ON. When set to OFF, the INTRO is not visible.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>SPIN</h3>
            <h3>SPACEBAR SPIN</h3>
            The default setting for SPACEBAR SPIN is ON. When set to OFF, it is not possible to spin the reels using SPACEBAR key.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>SOUND</h3>
            <h3>GAME SOUNDS</h3>
            The default setting for GAME SOUNDS is OFF. When set to OFF, only the SOUND EFFECTS are disabled.
          </div>
        </div>
  
        <div class="rowBlock  decor">
          <div class="rowTextBlock">
            <h3>BACKGROUND SOUNDS</h3>
            If the game has BACKGROUND MUSIC, the dafault setting for BACKGROUND MUSIC is OFF. When set to OFF, only the BACKGROUND MUSIC is disabled.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_AUTOSPIN_inactive.png" />
          </div>
          <div class="rowTextBlock">
            <h3>AUTO SPIN SETTINGS</h3>
            <h3>STOP ON BONUS</h3>
            The default setting for STOP ON BINUS is ON. When set to On, user interaction is required to start a bonus feature or to resume base game play when such a feature has finished. When set to OFF, bonus features start automatically and base game play is automatically resumed, when such a feature has been played. Start and end screens of bonus feature stay in view for 7 seconds before the game automatically starts a bonus feature or resumes base game play.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>TURBO SPIN</h3>
            Click/tap the TURBO SPIN button to active TURBO SPIN. The default setting for TURBO SPIN is OFF. When set to ON, the reels spin faster than normal spins.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>TOTAL SPINS</h3>
            Select the number of AUTO SPINS you want to play.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>LOSS LIMIT</h3>
            Stop AUTO SPIN if total amount of losses exceeds specified LOSS LIMIT amount.
          </div>
        </div>
  
        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>SINGLE WIN LIMIT</h3>
            Stop AUTO SPIN if any win exceeds specified SINGLE WIN LIMIT amount.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowTextBlock">
            <h3>START</h3>
            After selecting the preferred AUTO SPIN options, click/tap the START button to start AUTO SPIN.
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_paytable_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>PAYTABLE</h3>
          </div>
        </div>
  
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/rules/bttn_rules_regular.png" />
          </div>
          <div class="rowTextBlock">
            <h3>GAME RULES</h3>
          </div>
        </div>
      `,
      paytable: `
        <div class="rowBlock decor">
          <div class="rowImageBlock">
            <img src="${this.currentURL}assets/paytable/paytable_sym_12_Full2.png" />
          </div>
          <div class="rowTextBlock">
            <h3>
              STACKED MYSTERY SYMBOLS
            </h3>
            STACKED MYSTERY SYMBOLS are groups of three (3) symbols stacked on top of each other on the same reel. The three (3) symbols in a STACKED MYSTERY SYMBOL form a giant picture that covers the entire reel when the whole STACKED MYSTERY SYMBOL is visible on the reel. Depending on where a group of STACKED MYSTERY SYMBOLS lands on a reel, you could see one (1), two (2), or three (3) symbols on the reel that are part of the same STACKED MYSTERY SYMBOL. One of the regular symbols on the reels will be randomly selected to be the mystery symbol for that spin. All of the symbols in a STACKED MYSTERY SYMBOL that are visible on a reel will transform into the same regular symbol that was selected to be the mystery symbol for that spin. If more than one (1) STACKED MYSTERY SYMBOL lands on the reels on the same spin, all of the STACKED MYSTERY SYMBOLS that are visible on the reels will change into the same, matching regular symbol. STACKED MYSTERY SYMBOLS are on reels 1, 2, 3, 4, and 5.
          </div>
        </div>

        <div class="rowBlock decor">
          <div class="rowTextBlock payout">
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_09.png">
              <div>
                5. 5000<br>
                4. 500<br>
                3. 100
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_08.png">
              <div>
                5. 400<br>
                4. 200<br>
                3. 80
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_07.png">
              <div>
                5. 300<br>
                4. 100<br>
                3. 40
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_06.png">
              <div>
                5. 300<br>
                4. 100<br>
                3. 40
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_05.png">
              <div>
                5. 200<br>
                4. 40<br>
                3. 20
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_04.png">
              <div>
                5. 200<br>
                4. 40<br>
                3. 20
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_03.png">
              <div>
                5. 100<br>
                4. 30<br>
                3. 10
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_02.png">
              <div>
                5. 100<br>
                4. 30<br>
                3. 10
              </div>
            </div>
            <div class="symbolPayoutBlock">
              <img src="${this.currentURL}assets/paytable/paytable_sym_01.png">
              <div>
                5. 100<br>
                4. 30<br>
                3. 10
              </div>
            </div>
          </div>
        </div>

        <div class="rowBlock">
          <div class="rowTextBlock">
            <h3>
              WIN LINES
            </h3>
            <br>
            Win lines pay from left to right only.
            <br><br>
            <img src="${this.currentURL}assets/paytable/paytable_winlines.png" style="width: 100%;"/>
          </div>

        </div>
      `,

      currentLayout: LayoutName.layoutMain,
      // @ts-ignore not used by wrapper, just for game
      totalBetMultiplier: 1,
    };
    this.assets = {
      additionalPreloaderLogo: '',
      bgMenus: this.currentURL + 'assets/images/BG_blur_menus_luckygems.jpg',
      rulesImages: [
        this.currentURL + 'assets/rules/bttn_AUTOSPIN_inactive.png',
        this.currentURL + 'assets/rules/bttn_AUTOSPIN_regular.png',
        this.currentURL + 'assets/rules/bttn_CHOOSE_BET_COIN_regular.png',
        this.currentURL + 'assets/rules/bttn_FAST_SPIN_regular.png',
        this.currentURL + 'assets/rules/bttn_MENU_regular.png',
        this.currentURL + 'assets/rules/bttn_menuClose_regular.png',
        this.currentURL + 'assets/rules/bttn_MIN_regular.png',
        this.currentURL + 'assets/rules/bttn_paytable_regular.png',
        this.currentURL + 'assets/rules/bttn_PLUS_regular.png',
        this.currentURL + 'assets/rules/bttn_rules_regular.png',
        this.currentURL + 'assets/rules/bttn_settings_regular.png',
        this.currentURL + 'assets/rules/bttn_SOUND_OFF_regular.png',
        this.currentURL + 'assets/rules/bttn_SPIN_regular.png',
        this.currentURL + 'assets/rules/bttn_SUPER_STAKE2_active_regular.png',
      ],
      paytableImages: [
        this.currentURL + 'assets/paytable/paytable_sym_01.png',
        this.currentURL + 'assets/paytable/paytable_sym_02.png',
        this.currentURL + 'assets/paytable/paytable_sym_03.png',
        this.currentURL + 'assets/paytable/paytable_sym_04.png',
        this.currentURL + 'assets/paytable/paytable_sym_05.png',
        this.currentURL + 'assets/paytable/paytable_sym_06.png',
        this.currentURL + 'assets/paytable/paytable_sym_07.png',
        this.currentURL + 'assets/paytable/paytable_sym_08.png',
        this.currentURL + 'assets/paytable/paytable_sym_09.png',
        this.currentURL + 'assets/paytable/paytable_sym_12_Full2.png',
        this.currentURL + 'assets/paytable/paytable_winlines.png',
      ],
    };
  }
}

const model = new SlotModel();

export { model };
