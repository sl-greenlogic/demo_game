// The events that the machine handles
import { AdditionSectionType } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/AdditionSectionType';

export type UIEvent =
  | { type: 'INTRO' }
  | { type: 'IDLE' }
  | { type: 'PAYTABLE' }
  | { type: 'SPINNING' }
  | { type: 'SHOW_RESULT' };

export type IModelChangeEvent = { [key in keyof GeneralSettings]?: any };

export enum Buttons {
  BUTTON_SPIN = 'BUTTON_SPIN',
  BUTTON_MENU = 'BUTTON_MENU',
  BUTTON_BET_SETTINGS = 'BUTTON_BET_SETTINGS',
  BUTTON_BET_PLUS = 'BUTTON_BET_PLUS',
  BUTTON_BET_MINUS = 'BUTTON_BET_MINUS',
  BUTTON_FAST_SPIN = 'BUTTON_FAST_SPIN',
  BUTTON_MAX_BET = 'BUTTON_MAX_BET',
  BUTTON_AUTO_SPIN = 'BUTTON_AUTO_SPIN',
  BUTTON_SOUNDS = 'BUTTON_SOUNDS',
  BUTTON_SUPERSTAKE = 'BUTTON_SUPERSTAKE',
  BUTTON_HOME = 'BUTTON_HOME',
  BUTTON_MENU_CLOSE = 'BUTTON_MENU_CLOSE',
  BUTTON_STOP_ON_BONUS = 'BUTTON_STOP_ON_BONUS',
  BUTTON_START_AUTOPLAY = 'BUTTON_START_AUTOPLAY',
  BUTTON_LOSS_LIMIT_DOWN = 'BUTTON_LOSS_LIMIT_DOWN',
  BUTTON_LOSS_LIMIT_UP = 'BUTTON_LOSS_LIMIT_UP',
  BUTTON_SET_AUTO_PLAY_OPTION = 'BUTTON_SET_AUTO_PLAY_OPTION',
  BUTTON_SINGLE_WIN_DOWN = 'BUTTON_SINGLE_WIN_DOWN',
  BUTTON_SINGLE_WIN_UP = 'BUTTON_SINGLE_WIN_UP',
  BUTTON_PAYTABLE = 'BUTTON_PAYTABLE',
  BUTTON_RULES = 'BUTTON_RULES',
  BUTTON_SETTINGS = 'BUTTON_SETTINGS',
  BUTTON_TOGGLE_INTRO_SCREEN = 'BUTTON_TOGGLE_INTRO_SCREEN',
  BUTTON_TOGGLE_GAMBLE = 'BUTTON_TOGGLE_GAMBLE',
  BUTTON_TOGGLE_SPACESBAR_SPIN = 'BUTTON_TOGGLE_SPACESBAR_SPIN',
  BUTTON_TOGGLE_TOUCH_ANYWHERE_SPIN = 'BUTTON_TOGGLE_TOUCH_ANYWHERE_SPIN',
  BUTTON_TOGGLE_SLIDABLE_TOUCH = 'BUTTON_TOGGLE_SLIDABLE_TOUCH',
  BUTTON_TOGGLE_SOUNDS = 'BUTTON_TOGGLE_SOUNDS',
  BUTTON_TOGGLE_MUSIC = 'BUTTON_TOGGLE_MUSIC',
  BUTTON_CONTINUE = 'BUTTON_CONTINUE',
  BUTTON_BET_OPTION = 'BUTTON_BET_OPTION',
  BUTTON_DIALOG_YES = 'BUTTON_DIALOG_YES',
  BUTTON_DIALOG_NO = 'BUTTON_DIALOG_NO',
}

export interface ComponentState {
  enabled?: boolean;
  visible?: boolean;
  switchedOn?: boolean;
  value?: string;
}

export interface IState {
  popups?: Array<any>;
  customWidth?: number;
  customHeight?: number;
  additionSectionConfig?: {
    type: AdditionSectionType;
    config: any;
  };
  isGameLocked: boolean;
  isFunMode: boolean;
  currency?: string;

  useLayoutSet: number;
  currencyDelimiter: number;

  betTotal: number | null;
  balance: number;

  serverName: string;
  serverVersion: string;

  rtp: string;
  maxWin: number;
  maxWinProbability: string;

  bet: number;
  totalBetMultiplier: number;
  betRange: number[];

  isSuperstakeEnabled: boolean;

  isFastSpinEnabled: boolean;

  gameMessage: string;
  freespinsValue: string;
  showFreespinsBlock: boolean;
  totalWinValue: number;
  showTotalWinBlock: boolean;
  showTotalWinBlockBig: boolean;
  copyrights: string;

  isFastSpinToggled: boolean;

  currentLayout: string;
  clock: string;
  homeURL: string;

  isAutoplayEnabled: boolean;
  autoplayCount: number;
  isAutoplayStopOnBonus: boolean;
  autoplayRange: number[];
  autoplayLossLimit: number;
  autoplayLossLimitRange: number[];
  autoplaySingleWinLimit: number;
  autoplaySingleWinLimitRange: number[];
  autospinsLeft: string;

  hasIntroScreen: boolean;
  hasGamble: boolean;
  isSpacebarSpin: boolean;
  isTouchAnywhereSpin: boolean;
  isSlidableTouch: boolean;

  isMuted: boolean;
  isSoundEnabled: boolean;
  isMusicEnabled: boolean;

  rules: string;
  paytable: string;

  isDialogWindowEnabled: boolean;
  dialogWindowMessage: string;
  dialogWindowButtonNames: string[];

  lang?: string;
  ukgc: number;

  gameName: string;
  gameVersion: string;
}

export type AssetsConfig = {
  [key: string]: string | string[];

  bgMenus: string;
  additionalPreloaderLogo: string;
  rulesImages: string[];
  paytableImages: string[];
};

export type GeneralSettings = IState & {
  clock: string;
  currentState: string;
  currentLayout: string;
};

export type TranslationConfig = {
  [key: string]: string;
};
