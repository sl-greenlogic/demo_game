import { model } from './model/SlotModel';
import { GcwEventsController } from './sdk/core/gcw/gcwEventsController';
import { SlotGame } from './sdk/core/gametypes/slotgame/slotGame';
import { App } from './app';
// import { Howler } from 'howler';
import { IModelChangeEvent } from './model/IFSM';
import { GameMessageType } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/GameMessageType';
// import {IAdditionSectionType} from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/IAdditionSectionType';
import { LayoutName } from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/LayoutName';
// import {Regulations} from "@stakelogic_greenlogic/enriched_wrapper_types/interfaces/Regulations";

// import {AdditionSectionType} from '@stakelogic_greenlogic/enriched_wrapper_types/interfaces/AdditionSectionType';

export class ExternalSlotController {
  private static instance: ExternalSlotController;
  public initConf: any = {
    gamble: null,
    intro_screen: null,
    fast_spin: null,
    locale: null,
    slideable_touch: null,
    spacebar_spin: null,
    touch_anywhere: null,
    ukgc: null,
  };
  // private counterSpin = 0;
  private _model = model;
  // private lastTopPositions = this._model.state.additionSectionConfig.config[0].topPositions.length;

  protected _game: SlotGame | undefined;
  protected gameModel: any = {};
  private _gameMessage: any = [
    {
      msg: 'ACTIVATE TURBO SPIN FOR FASTER GAMEPLAY',
      type: GameMessageType.string,
    },
    { msg: 'GOOD LUCK', type: GameMessageType.string },
    {
      msg: 'ACTIVATE SUPER STAKE MODE FOR EXTRA FEATURES!',
      type: GameMessageType.string,
    },
    {
      msg: ' TEST LONG MESSAGE GOOD LUCK AND ACTIVATE SUPER STAKE MODE  MESSAGE GOOD LUCK AND ACTIVATE SUPER  FOR EXTRA FEATURES!',
      type: GameMessageType.string,
    },
    { msg: 'TAP ANYWHERE TO STOP THE REELS', type: GameMessageType.string },
    {
      msg: {
        img: '/images/LV1_static.png',
        winValue: 5,
        multiplier: 3,
        lineWinValue: 20,
        // showWinnerText: false,
      },
      type: GameMessageType.winTemplate,
    },
    {
      msg: `<div>
          <style type="text/css">
              .win {
                  display: flex;
                  justify-content: center;
                  align-items: center;
              }

              .winImg {
                  margin-left: 10px;
                  height: 50px;
              }

          </style>
            <div class="win"> 
                <div>WIN: 10</div>
                <img class="winImg" src="/images/LV1_static.png"/>
            </div>
      </div>
      `,
      type: GameMessageType.html,
    },
    {
      msg: `
      <div>
          <style type="text/css">
        @keyframes cf4FadeInOut {
            0% {
                opacity:1;
            }
            17% {
                opacity:1;
            }
            25% {
                opacity:0;
            }
            92% {
                opacity:0;
            }
            100% {
                opacity:1;
            }
        }

        #cf4a img:nth-of-type(1) {
            animation-delay: 6s;
        }
        #cf4a img:nth-of-type(2) {
            animation-delay: 4s;
        }
        #cf4a img:nth-of-type(3) {
            animation-delay: 2s;
        }
        #cf4a img:nth-of-type(4) {
            animation-delay: 0s;
        }

        #cf4a {
            position: relative;
            height: 90px;
            width: 90px;
            margin: 0 auto;
        }
        #cf4a img {
            height: 90px;
            width: 90px;
            position: absolute;
            left: 0;
            animation-name: cf4FadeInOut;
            animation-timing-function: ease-in-out;
            animation-iteration-count: infinite;
            animation-duration: 8s;
        }

        .messageFromGame {
            display: flex;
            height: 100px !important;
            width: 100%;
            align-items: center;
            justify-content: center;
            font-size: 30px;
            margin: 0px 6px;
            color: white;
        }

          </style>
          <div class="messageFromGame">
            <div>
            TAP ANYWHERE TO STOP THE REELS
            </div>
          <div id="cf4a">
            <img src="/images/CO1_static.png" />
            <img src="/images/HV1_static.png" />
            <img src="/images/HV2_static.png" />
          </div>  
        </div>
      </div>
`,
      type: GameMessageType.html,
    },
  ];

  // private inter: NodeJS.Timeout = null;
  private static getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  private static gambledConfig = (type: number): any => {
    switch (type) {
      case 1: {
        return {
          BUTTON_COLLECT: { enabled: true, visible: true },
          BUTTON_GAMBLE: { enabled: true, visible: true },
          BUTTON_SUPERSTAKE: { enabled: true, visible: true },
        };
      }
      case 2: {
        return {
          BUTTON_COLLECT: { enabled: true, visible: true },
          BUTTON_GAMBLE: { enabled: false, visible: true },
        };
      }
      default:
        return {
          BUTTON_COLLECT: { enabled: false, visible: false },
          BUTTON_GAMBLE: { enabled: false, visible: false },
        };
    }
  };

  private handlers: { [key: string]: Function } = {
    BUTTON_FAST_SPIN: () => {
      this._model.state.isFastSpinEnabled =
        !this._model.state.isFastSpinEnabled;
      this.onModelUpdate({
        general: { isFastSpinEnabled: this._model.state.isFastSpinEnabled },
      });
    },

    BUTTON_MAX_BET: () => {
      const betRange = this._model.state.isSuperstakeEnabled
        ? this._model.state.betRange.map((amount) => amount * 2)
        : this._model.state.betRange;
      const bet = betRange[betRange.length - 1];
      this._model.state.bet = this._model.state.isSuperstakeEnabled
        ? bet / 2
        : bet;
      this.onModelUpdate({
        general: {
          bet: this._model.state.bet,
        },
      });
    },

    BUTTON_SUPERSTAKE: () => {
      this._model.state.isSuperstakeEnabled =
        !this._model.state.isSuperstakeEnabled;
      this.onModelUpdate({
        general: {
          isSuperstakeEnabled: this._model.state.isSuperstakeEnabled,
          betTotal: this._model.state.isSuperstakeEnabled
            ? this._model.state.bet * 2
            : null,
          betRange: this._model.state.isSuperstakeEnabled
            ? this._model.state.betRange.map((amount) => amount * 2)
            : this._model.state.betRange,
        },
      });
    },

    BUTTON_SOUNDS: () => {
      // const isMuted = !this._model.state.isMuted;
      // this._model.state.isMuted = isMuted;
      // this._model.state.isSoundEnabled = !isMuted;
      // this._model.state.isMusicEnabled = !isMuted;
      this.onModelUpdate({
        general: {
          // isMuted: isMuted,
          isSoundEnabled: this._model.state.isSoundEnabled,
          isMusicEnabled: this._model.state.isMusicEnabled,
        },
      });

      // Howler.mute(isMuted);
    },

    BUTTON_SPIN: () => {
      const newBalance = this._model.state.balance - this._model.state.bet;

      if (newBalance >= 0) {
        this._model.state.balance = newBalance;

        const msgO =
          this._gameMessage[ExternalSlotController.getRandomInt(0, 6)];

        // @ts-ignore
        const gembledConfig = ExternalSlotController.gambledConfig(
          ExternalSlotController.getRandomInt(1, 2)
        );
        this.onModelUpdate({
          elements: {
            BUTTON_SPIN: { enabled: false, visible: false },
            BUTTON_SPIN_STOP: { enabled: true, visible: true },
            ...gembledConfig,
          },
          general: {
            balance: newBalance,
            gameMessage: msgO.msg,
            gameMessageType: msgO.type,
          },
        });
        // let min = 726.42;
        // let max = 1600000;
        // let percMin = 0;
        // let percMax = 5;
        // this.inter = setInterval(() => {
        //   min += 1000;
        //   max += 100000;
        //   percMin += 5;
        //   percMax += 10;
        //   if (max > 1800000) {
        //     clearInterval(this.inter);
        //     this.onModelUpdate({
        //       general: {
        //         additionSectionConfig: {
        //           config:{
        //             "miniJackpotMaxConfig":{"currency":"","amount":5000},
        //             "miniJackpotAmount": 100,
        //             "miniJackpotPercent": percMin,
        //             "grandJackpotAmount": 100,
        //             "grandJackpotPercent": percMax,
        //             "logo":"https://cdn.dev02-gs-stakelogic.com/libs/stakelogic/ew-0.1.18.3/img/buttons/bttn_MENU_SUPER_push.png",
        //             "miniJackpotWinners":[
        //               {
        //
        //                 date: '17/02/21',
        //                 time: '13:28',
        //                 amount: '5000',
        //                 currency: null
        //               }
        //             ]},
        //           type : IAdditionSectionType.GrandJackpot
        //         }
        //       }
        //     });
        //     return;
        //   }
        //   this.onModelUpdate({
        //     general: {
        //       additionSectionConfig: {
        //         config:{
        //           "miniJackpotMaxConfig":{"currency":"","amount":5000},
        //           "miniJackpotAmount":min,
        //           "miniJackpotPercent": percMin,
        //           "grandJackpotAmount": max,
        //           "grandJackpotPercent": percMax,
        //           "logo":"https://cdn.dev02-gs-stakelogic.com/libs/stakelogic/ew-0.1.18.3/img/buttons/bttn_MENU_SUPER_push.png",
        //           "miniJackpotWinners":[
        //             {
        //
        //               date: '17/02/21',
        //               time: '13:28',
        //               amount: '5000',
        //               currency: null
        //             }
        //           ]},
        //         type : IAdditionSectionType.GrandJackpot
        //       }
        //     }
        //   });
        //
        // }, 2000)
        if (this._game) {
          this._game.startSpin();
        }
      } /*else {
                this.counterSpin = this.counterSpin + 1;
                if (this.counterSpin > 1 && this.lastTopPositions > 1) {
                    this.lastTopPositions = this.lastTopPositions - 1
                }
                this.onModelUpdate({
                    general: {
                        additionSectionConfig: {
                            ...this._model.state.additionSectionConfig,
                            config: [{
                                ...this._model.state.additionSectionConfig.config[0],
                                playerPosition: {
                                    ...this._model.state.additionSectionConfig.config[0].playerPosition,
                                    position: this.lastTopPositions
                                },
                                // topPositions: [...this._model.state.additionSectionConfig.config[0].topPositions,
                                //     {
                                //         "position": this.counterSpin,
                                //         "value": 336.5,
                                //         "win": 5683.393,
                                //         "username": "RGS***"
                                //     },
                                // ]
                            }, {...this._model.state.additionSectionConfig.config[1]}],
                        }
                    },
                });
            }*/
    },

    BUTTON_SINGLE_WIN_UP: () => {
      this.onModelUpdate({
        general: {
          autoplaySingleWinLimit:
            (this._model.state.autoplaySingleWinLimit += 1),
        },
      });
    },
    BUTTON_SINGLE_WIN_DOWN: () => {
      this.onModelUpdate({
        general: {
          autoplaySingleWinLimit:
            (this._model.state.autoplaySingleWinLimit -= 1),
        },
      });
    },

    BUTTON_LOSS_LIMIT_UP: () => {
      this.onModelUpdate({
        general: {
          autoplayLossLimit: (this._model.state.autoplayLossLimit += 1),
        },
      });
    },

    BUTTON_LOSS_LIMIT_DOWN: () => {
      this.onModelUpdate({
        general: {
          autoplayLossLimit: (this._model.state.autoplayLossLimit -= 1),
        },
      });
    },

    BUTTON_START_AUTOPLAY: () => {
      this._model.state.isAutoplayEnabled = true;
      this._model.state.autospinsLeft =
        this._model.state.autoplayCount > 0
          ? this._model.state.autoplayCount.toString()
          : '∞';
      this._model.state.currentLayout = LayoutName.layoutMain;

      this.onModelUpdate({
        general: {
          isAutoplayEnabled: this._model.state.isAutoplayEnabled,
          autospinsLeft: this._model.state.autospinsLeft,
          currentLayout: this._model.state.currentLayout,
        },
        elements: {
          BUTTON_AUTO_SPIN: { visible: false, enabled: false },
          BUTTON_AUTOPLAY_STOP: { visible: true, enabled: true },
        },
      });
    },
    BUTTON_AUTOPLAY_STOP: () => {
      this._model.state.isAutoplayEnabled = false;
      this._model.state.currentLayout = LayoutName.layoutMain;
      this.onModelUpdate({
        general: {
          isAutoplayEnabled: this._model.state.isAutoplayEnabled,
          autospinsLeft: this._model.state.autospinsLeft,
          currentLayout: this._model.state.currentLayout,
        },
        elements: {
          BUTTON_AUTOPLAY_STOP: { visible: false, enabled: false },
          BUTTON_AUTO_SPIN: { visible: true, enabled: true },
        },
      });
    },
    BUTTON_TOGGLE_INTRO_SCREEN: () => {
      this._model.state.hasIntroScreen = !this._model.state.hasIntroScreen;
      this.onModelUpdate({
        general: {
          hasIntroScreen: this._model.state.hasIntroScreen,
        },
      });
    },

    BUTTON_TOGGLE_GAMBLE: () => {
      this._model.state.hasGamble = !this._model.state.hasGamble;
      this.onModelUpdate({
        general: {
          hasGamble: this._model.state.hasGamble,
        },
      });
    },

    BUTTON_TOGGLE_SPACESBAR_SPIN: () => {
      this._model.state.isSpacebarSpin = !this._model.state.isSpacebarSpin;
      this.onModelUpdate({
        general: {
          isSpacebarSpin: this._model.state.isSpacebarSpin,
        },
      });
    },

    BUTTON_TOGGLE_TOUCH_ANYWHERE_SPIN: () => {
      this._model.state.isTouchAnywhereSpin =
        !this._model.state.isTouchAnywhereSpin;
      if (this._model.state.isTouchAnywhereSpin) {
        this._model.state.isSlidableTouch = false;
      }
      this.onModelUpdate({
        general: {
          isTouchAnywhereSpin: this._model.state.isTouchAnywhereSpin,
          isSlidableTouch: this._model.state.isSlidableTouch,
        },
      });
    },

    BUTTON_TOGGLE_SLIDABLE_TOUCH: () => {
      this._model.state.isSlidableTouch = !this._model.state.isSlidableTouch;
      if (this._model.state.isSlidableTouch) {
        this._model.state.isTouchAnywhereSpin = false;
      }
      this.onModelUpdate({
        general: {
          isTouchAnywhereSpin: this._model.state.isTouchAnywhereSpin,
          isSlidableTouch: this._model.state.isSlidableTouch,
        },
      });
    },

    BUTTON_TOGGLE_SOUNDS: () => {
      const isSoundEnabled = !this._model.state.isSoundEnabled;
      this._model.state.isSoundEnabled = isSoundEnabled;

      // if (isSoundEnabled || this._model.state.isMusicEnabled) {
      //   this._model.state.isMuted = false;
      // } else if (!isSoundEnabled && !this._model.state.isMusicEnabled) {
      //   this._model.state.isMuted = true;
      // }

      // Howler.mute(this._model.state.isMuted);

      this.onModelUpdate({
        general: {
          // isMuted: this._model.state.isMuted,
          isSoundEnabled: isSoundEnabled,
        },
      });
    },

    BUTTON_TOGGLE_MUSIC: () => {
      const isMusicEnabled = !this._model.state.isMusicEnabled;
      this._model.state.isMusicEnabled = isMusicEnabled;

      // if (isMusicEnabled || this._model.state.isSoundEnabled) {
      //   this._model.state.isMuted = false;
      // } else if (!isMusicEnabled && !this._model.state.isSoundEnabled) {
      //   this._model.state.isMuted = true;
      // }

      // Howler.mute(this._model.state.isMuted);

      this.onModelUpdate({
        general: {
          // isMuted: this._model.state.isMuted,
          isMusicEnabled: isMusicEnabled,
        },
      });
    },

    BUTTON_BET_MINUS: () => {
      const filteredRange = this._model.state.betRange.filter(
        (bet) => bet < this._model.state.bet
      );
      if (filteredRange.length) {
        this._model.state.bet = Math.max(...filteredRange);
        this.onModelUpdate({
          general: {
            bet: this._model.state.bet,
            betTotal: this._model.state.isSuperstakeEnabled
              ? this._model.state.bet * 2
              : null,
          },
        });
      }
    },

    BUTTON_BET_PLUS: () => {
      const filteredRange = this._model.state.betRange.filter(
        (bet) => bet > this._model.state.bet
      );
      if (filteredRange.length) {
        this._model.state.bet = Math.min(...filteredRange);
        this.onModelUpdate({
          general: {
            bet: this._model.state.bet,
            betTotal: this._model.state.isSuperstakeEnabled
              ? this._model.state.bet * 2
              : null,
          },
        });
      }
    },

    BUTTON_SET_AUTO_PLAY_OPTION: (data: any) => {
      this._model.state.autoplayCount = parseFloat(data.count);

      this.onModelUpdate({
        general: {
          autoplayCount: this._model.state.autoplayCount,
        },
      });
    },

    BUTTON_BET_OPTION: (data: any) => {
      const count = parseFloat(data.count);

      this._model.state.bet = this._model.state.isSuperstakeEnabled
        ? count / 2
        : count;
      this._model.state.betTotal = this._model.state.isSuperstakeEnabled
        ? count
        : null;
      this.onModelUpdate({
        general: {
          bet: this._model.state.bet,
          betTotal: this._model.state.betTotal,
        },
      });
    },

    BUTTON_STOP_ON_BONUS: (data: any) => {
      this._model.state.isAutoplayStopOnBonus =
        !this._model.state.isAutoplayStopOnBonus;

      this.onModelUpdate({
        general: {
          isAutoplayStopOnBonus: this._model.state.isAutoplayStopOnBonus,
        },
      });
    },

    BUTTON_HOME: (data: any) => {
      GcwEventsController.emitGcwEvent(gcw.api.GameEventType.HOME);
    },
    OPEN_AUTOPLAY_SETTING: () => {
      this.hideMessageInWrapper();
    },
    OPEN_PAYTABLE: () => {
      this.hideMessageInWrapper();
    },
    OPEN_GENERAL_SETTINGS: () => {
      this.hideMessageInWrapper();
    },
    OPEN_BET_SETTINGS: () => {
      this.hideMessageInWrapper();
    },
    OPEN_AUTOPLAY_SETTINGS: () => {
      this.hideMessageInWrapper();
    },
    BUTTON_ADDITION_SECTION_SHOW: () => {},
    BUTTON_ADDITION_SECTION_HIDE: () => {},
  };
  private hideMessageInWrapper = () => {
    this.onModelUpdate({
      general: {},
    });
  };

  private constructor() {
    GcwEventsController.subscribeForGcwEvent(
      'UI_ON_INITIALIZE',
      this.initUI.bind(this),
      false
    );

    GcwEventsController.subscribeForGcwEvent(
      'ON_BUTTON_CLICKED',
      this.onButtonClicked.bind(this),
      false
    );
  }

  public static getInstance(): ExternalSlotController {
    if (!this.instance) {
      this.instance = new ExternalSlotController();
    }

    return this.instance;
  }

  setGame(game: SlotGame): void {
    this._game = game;
  }

  initUI(): void {
    // Update model with initial config
    let parameters: any = {};
    if (this.initConf !== null) {
      parameters['hasGamble'] = this.initConf.gamble;
      parameters['hasIntroScreen'] = this.initConf.intro_screen;
      parameters['isFastSpinEnabled'] = this.initConf.fast_spin;
      parameters['lang'] = this.initConf.locale;
      parameters['isSlidableTouch'] = this.initConf.slideable_touch;
      parameters['isSpacebarSpin'] = this.initConf.spacebar_spin;
      parameters['isTouchAnywhereSpin'] = this.initConf.touch_anywhere;
      parameters['ukgc'] = this.initConf.ukgc;
      parameters['currencyCode'] = 'EUR';
      parameters['regulation'] = { nlLicense: false };
      this.onModelInitial(parameters);
    }
    this.setAssets();
    this.syncState();
    this.syncElement();

    this.setClock();
    App.uiInit();
  }

  syncElement(): void {
    this.onModelUpdate({
      elements: {
        BUTTON_SPIN: { enabled: true, visible: true },
        BUTTON_MENU: { enabled: true, visible: true },
        BUTTON_BET_SETTINGS: { enabled: true, visible: true },
        BUTTON_BET_PLUS: { enabled: true, visible: true },
        BUTTON_BET_MINUS: { enabled: true, visible: true },
        BUTTON_FAST_SPIN: { enabled: true, visible: true },
        BUTTON_MAX_BET: { enabled: true, visible: true },
        BUTTON_SET_AUTO_PLAY_OPTION: { enabled: true, visible: true },
        BUTTON_BET_OPTION: { enabled: true, visible: true },
        BUTTON_AUTO_SPIN: { enabled: true, visible: true },
        BUTTON_STOP_ON_BONUS: { enabled: true, visible: true },
        BUTTON_SOUNDS: { enabled: true, visible: true },
        BUTTON_SUPERSTAKE: { enabled: true, visible: true },
        BUTTON_LOSS_LIMIT_DOWN: { enabled: true, visible: true },
        BUTTON_LOSS_LIMIT_UP: { enabled: true, visible: true },
        BUTTON_SINGLE_WIN_DOWN: { enabled: true, visible: true },
        BUTTON_SINGLE_WIN_UP: { enabled: true, visible: true },
        WRAPPER: { enabled: true, visible: true },
        BUTTON_SPIN_STOP: { enabled: true, visible: false },
        BUTTON_CONTINUE: { enabled: true, visible: true },
        BUTTON_GAMBLE: { enabled: false, visible: false },
        BUTTON_COLLECT: { enabled: false, visible: false },
        BUTTON_ADDITION_SECTION: { enabled: true, visible: true },
        BUTTON_TOGGLE_INTRO_SCREEN: { enabled: true, visible: true },
        BUTTON_TOGGLE_GAMBLE: { enabled: true, visible: true },
        BUTTON_TOGGLE_TOUCH_ANYWHERE_SPIN: { enabled: true, visible: true },
        BUTTON_TOGGLE_SLIDABLE_TOUCH: { enabled: true, visible: true },
        BUTTON_TOGGLE_SPACESBAR_SPIN: { enabled: true, visible: true },
        BUTTON_TOGGLE_MUSIC: { enabled: true, visible: true },
        BUTTON_TOGGLE_SOUNDS: { enabled: true, visible: true },
        BUTTON_START_AUTOPLAY: { enabled: true, visible: true },
        BUTTON_AUTOPLAY_STOP: { enabled: false, visible: false },
        BUTTON_MENU_CLOSE: { enabled: true, visible: true },
        BUTTON_HOME: { enabled: true, visible: true },
        BUTTON_OPEN_GENERAL_SETTINGS: { enabled: true, visible: true },
        BUTTON_OPEN_RULES_SECTION: { enabled: true, visible: true },
        BUTTON_OPEN_INFO_SECTION: { enabled: true, visible: true },
        GAME_MESSAGE_PORTRAIT: {
          enabled: true,
          visible: true,
          bottom: 200,
          overrideCss: true,
        },
      },
    });
  }

  syncState(): void {
    // console.error('sync state', this._model.state);
    this.onModelUpdate({ general: this._model.state });
  }

  setAssets(): void {
    // console.error('update assets', this._model.assets);
    this.onModelUpdate({ assets: this._model.assets });
  }

  setClock(): void {
    function startTime(): string {
      const today = new Date();
      const h: number = today.getHours();
      const m: number = today.getMinutes();
      const resM = checkTime(m);

      return h + ':' + resM;
    }

    // add zero in front of numbers < 10
    function checkTime(i: number): string {
      let result: string = i.toString();
      if (i < 10) {
        result = '0' + i;
      }

      return result;
    }
    this._model.state.clock = startTime();
    this.onModelUpdate({
      general: { clock: this._model.state.clock + ` AM` },
      // regulations:  this._model.state.regulations.length < 0 ? this._model.state.regulations.push(Regulations.UKGC) : this._model.state.regulations.length = 0
    });

    setInterval(() => {
      this._model.state.clock = startTime();
      this.onModelUpdate({
        general: {
          clock: this._model.state.clock + ` AM`,
          // regulations:  this._model.state.regulations.length < 0 ? this._model.state.regulations.push(Regulations.UKGC) : this._model.state.regulations.length = 0
        },
      });
    }, 60000);
  }

  private onButtonClicked(params: any = {}) {
    const { name, data } = params;
    const handler = this.handlers[name];
    if (handler) {
      handler(data);
    }
  }

  public updateModel(data: IModelChangeEvent) {
    Object.entries(data).forEach(([key, value]) => {
      // @ts-ignore
      this._model.state[key] = value;
    });

    this.onModelUpdate({ general: data });
  }

  public getParameter(key: string): any {
    // @ts-ignore
    return this._model.state[key];
  }
  private onModelInitial(params: any) {
    GcwEventsController.emitGcwEvent('INITIAL_MODEL', params);
  }

  private onModelUpdate(params: any) {
    GcwEventsController.emitGcwEvent('UPDATE_MODEL', params);
  }

  public updateModelFromMessage(message: any): void {
    if (message.game) {
      const game = message.game;
      if (!this.gameModel) {
        this.updateModel({
          isFunMode: !!game.currentWallet.fm,
          currency: game.currentWallet.currencySymbol,
          currencyDelimiter: game.currentWallet.decimalSpaces,
          balance: game.currentWallet.fm || game.currentWallet.rm,
        });
      } else {
        this.updateModel({
          balance: game.walletAfterLastBet.fm || game.walletAfterLastBet.rm,
        });
      }
      this.gameModel = {
        currentWallet: game.currentWallet,
        walletAfterLastBet: game.walletAfterLastBet,
      };
    }
  }

  public updateModelWalletToLastState() {
    this.updateModel({
      balance:
        this.gameModel.currentWallet.fm || this.gameModel.currentWallet.rm,
    });
  }
}
