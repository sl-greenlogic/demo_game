/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import * as PIXI from 'pixi.js';
import 'pixi-spine';
import {
  ScreenSettings,
  Scenes,
  GameSettings,
  GAMENAME,
  GAMEVERSION,
} from './settings/game.settings';
import {
  SceneController,
  SceneLayer,
} from './sdk/core/controllers/sceneController';
import { GameDebugger } from './sdk/core/debugger';
import { ExternalSlotController } from './ExternalSlotController';

PIXI.utils.skipHello();

export class App extends PIXI.Application {
  public static initialized: boolean = false;
  private static instance: App;
  private static _sceneController: SceneController;
  private static _debugger: GameDebugger;

  private constructor() {
    super({
      antialias: false,
      backgroundColor: 0x000000,
      roundPixels: true,
      width: ScreenSettings.Size.width,
      height: ScreenSettings.Size.height,
    });
  }

  // Create singlenton instance
  public static get application(): App {
    if (!this.instance) {
      this.instance = new App();
      this._debugger = new GameDebugger(GameSettings.Debug);
      this._debugger.loaded(GAMENAME + ' ' + GAMEVERSION);
    }

    return this.instance;
  }

  private addListeners(): void {
    window.addEventListener('resize', this.resize.bind(this));
    this.resize();
  }

  static init(): void {
    const gameCss = document.createElement('link');
    gameCss.type = 'text/css';
    gameCss.rel = 'stylesheet';
    gameCss.href =
      'https://cdn.dev02-gs-stakelogic.com/slsdk/luckygemsprototype/styles/style.css';
    document.getElementsByTagName('head')[0].appendChild(gameCss);

    const webfontScript = document.createElement('script');
    webfontScript.type = 'text/javascript';
    webfontScript.src =
      'https://cdn.dev02-gs-stakelogic.com/slsdk/luckygemsprototype/libs/webfont.js';
    document.getElementsByTagName('head')[0].appendChild(webfontScript);

    const gameElement = document.getElementsByTagName('gcw-game')[0];
    const gameDiv = document.createElement('div');
    gameDiv.id = 'game';
    gameDiv.style.cssText =
      'position: fixed; top: 0px; left: 0px; width: 1440px; height: 720px;';
    gameElement.appendChild(gameDiv);

    const divTarget = document.getElementById('game');

    if (divTarget) {
      divTarget.appendChild(this.instance.view);
    }

    this.instance.addListeners();
    this._sceneController = new SceneController();

    // make initialisation of listeners
    ExternalSlotController.getInstance();
  }

  static uiInit(): void {
    App.sceneController.loadScene(Scenes.BootScene, SceneLayer.UI);
  }

  static get sceneController(): SceneController {
    return this._sceneController;
  }

  static get debugger(): GameDebugger {
    return this._debugger;
  }

  private resize(): void {
    let gStyle: any = document.getElementById('game');
    let wWidth: number = window.innerWidth;
    let wHeight: number = window.innerHeight;
    // @ts-ignore
    let wRatio: number = wWidth / wHeight;
    let gRatio: number = ScreenSettings.Size.width / ScreenSettings.Size.height;
    // @ts-ignore
    let minRatio: number = 16 / 9;
    let width,
      height,
      left,
      top: number = 0;

    if (wRatio < gRatio) {
      if (wRatio >= minRatio) {
        // scale down to the minimum ratio (lose sides of the game)
        width = wHeight * gRatio;
        height = wHeight;
        top = (wHeight - height) / 2;
      } else {
        // scale down once minimumRatio has been reached (add top and bottom bars)
        height = wHeight;
        width = wHeight * gRatio;
        top = 0;
      }
      left = (wWidth - width) / 2;
    } else {
      // add side bars, as the full height and max width have been exceeded
      width = wHeight * gRatio;
      height = wHeight;
      left = (wWidth - wHeight * gRatio) / 2;
      top = 0;
    }

    // update css to match new size
    gStyle.style.width = width + 'px';
    gStyle.style.height = height + 'px';
    gStyle.style.left = left + 'px';
    gStyle.style.top = top + 'px';
  }
}

App.application;
