/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import {
  loadedFiles,
  imagesRes,
  Resources,
  audioSpriteSheetRes,
  spineRes,
  animatedSpritesRes,
  particlesRes,
} from '../settings/game.resources';
import {
  ScreenSettings,
  ControlEvents,
  GameEvents,
} from '../settings/game.settings';
import { TweenMax, Linear } from 'gsap';
import { ReelSetInstanceMap, TropicalReelSet } from './tropicalReelSet';
import * as particles from 'pixi-particles';
import { SlotGame } from '../sdk/core/gametypes/slotgame/slotGame';
import { Scene } from '../sdk/core/basecomponents/scene';
import { GameControl } from '../sdk/ui/gamecontrol';
import { App } from '../app';
import { SlotMachine, GameStates } from '../sdk/core/slotmachine';
import {
  SlotMachineSettings,
  ReelsSymbolsCodes,
} from '../settings/slotmachine.settings';
import { ReelSetStatus } from '../sdk/core/gametypes/slotgame/reelSet';
import { ReelResult } from '../sdk/core/gametypes/slotgame/reel';
import { GcwEventsController } from '../sdk/core/gcw/gcwEventsController';
import { model } from '../model/SlotModel';
import { ExternalSlotController } from '../ExternalSlotController';
import { stateConfig } from '../model/stateConfig';

export type FsmListeners = {
  [id: number]: string;
};

export class Tropical implements SlotGame {
  private backContainer!: PIXI.Container;
  private midContainer!: PIXI.Container;
  private frontContainer!: PIXI.Container;

  private gameTitle!: PIXI.spine.Spine;
  private coinAnimation!: PIXI.extras.AnimatedSprite;
  private titleParticleConfig!: any;
  private titleGoldPotParticleConfig!: any;

  private reelsSetsHashMap!: ReelSetInstanceMap;
  private playingLogoWin!: boolean;
  private playingLogoIdle!: boolean;

  constructor(private scene: Scene, private controls: GameControl) {
    this.reelsSetsHashMap = [];
    this.playingLogoWin = false;
    this.playingLogoIdle = false;

    this.titleParticleConfig = loadedFiles[particlesRes.title_glow].data;
    this.titleGoldPotParticleConfig =
      loadedFiles[particlesRes.title_goldPot].data;

    this.subscribeForFsmEvents();
    window.addEventListener('resize', this.onResize.bind(this));
  }

  private onResize() {
    if (window.innerHeight > window.innerWidth) {
      // is portrait
      this.gameTitle.scale.set(0.25);
      this.gameTitle.y = 63;
    } else {
      this.gameTitle.scale.set(0.7);
      this.gameTitle.y = 75;
    }

    console.log(this.gameTitle.scale);
  }
  // #region Interface Implementations
  setupGameLayers(): void {
    this.backContainer = new PIXI.Container();
    this.scene.sceneContainer.addChild(this.backContainer);
    this.midContainer = new PIXI.Container();
    this.scene.sceneContainer.addChild(this.midContainer);
    this.frontContainer = new PIXI.Container();
    this.scene.sceneContainer.addChild(this.frontContainer);
  }

  setupGameBackground(): void {
    const background = new PIXI.Sprite(
      loadedFiles[imagesRes.background_main].texture
    );
    background.anchor.set(0.5);
    background.position.set(ScreenSettings.Center.x, ScreenSettings.Center.y);
    this.backContainer.addChild(background);
  }

  setupGameForeGround(): void {
    this.gameTitle = new PIXI.spine.Spine(
      loadedFiles[spineRes.logo_spine].spineData
    );

    this.gameTitle.x = ScreenSettings.Center.x;
    this.gameTitle.state.setAnimation(0, 'logo_idle', false);
    this.frontContainer.addChild(this.gameTitle);
    this.titleElasticAnim();

    this.coinAnimation =
      Resources.animations[animatedSpritesRes.coins_anim].animation;
    this.coinAnimation.animationSpeed = 0.5;
    this.coinAnimation.loop = false;
    this.coinAnimation.x =
      ScreenSettings.Center.x - this.coinAnimation.width / 2;
    this.coinAnimation.y = ScreenSettings.Center.y + 100;
    this.coinAnimation.visible = false;
    this.frontContainer.addChild(this.coinAnimation);

    setTimeout(() => {
      this.onResize();
    }, 1000);
  }

  addControlsListeners(): void {
    this.controls.on(
      ControlEvents.changeAmountOfReels,
      this.onChangeAmountOfReelsSets.bind(this)
    );
  }

  addGameListeners(): void {
    for (const reelSetKey in this.reelsSetsHashMap) {
      this.reelsSetsHashMap[reelSetKey].on(
        GameEvents.reelSetSpinCompleted,
        this.onReelSetCompleted.bind(this)
      );
      this.reelsSetsHashMap[reelSetKey].on(
        GameEvents.reelSetPerformedPreStart,
        this.onReelSetPerformedPreStart.bind(this)
      );
    }
  }

  subscribeForFsmEvents(): void {
    SlotMachine.subscribeForFsmStatusChange({
      state: GameStates.Any,
      callback: this.onFsmStateChanged.bind(this),
      active: true,
    });
  }

  setupGame(): void {
    this.setupReelsSet(SlotMachineSettings.QuattroInstancesFromStart);
  }

  /**
   * Function that changes the amount of reels set
   * It can be triggered by player interaction or in the game start
   * @param {number} amount
   * @memberof Tropical
   */
  setupReelsSet(amount: number): void {
    let instantiated = false;

    if (amount === 1) {
      this.fadeOutCurrentReelsSet();
      this.reelsSetsHashMap = [];
      const reelSet = new TropicalReelSet(
        1,
        SlotMachineSettings.SingleReelSetPosition
      );
      this.reelsSetsHashMap[1] = reelSet;
      this.midContainer.addChild(reelSet);
      instantiated = true;
    } else {
      if (Object.keys(this.reelsSetsHashMap).length === 1) {
        this.fadeOutCurrentReelsSet();
        this.reelsSetsHashMap = [];
        for (let index = 1; index <= 4; index++) {
          const isActive = index <= amount ? true : false;
          const reelSet = new TropicalReelSet(
            index,
            SlotMachineSettings.MultipleReelSetPositions[index - 1],
            SlotMachineSettings.MultipleReelSetScale,
            isActive,
            0.5
          );
          this.reelsSetsHashMap[index] = reelSet;
          this.midContainer.addChild(reelSet);
        }
        instantiated = true;
      } else {
        for (
          let index = 1;
          index <= Object.keys(this.reelsSetsHashMap).length;
          index++
        ) {
          index > amount
            ? this.reelsSetsHashMap[index].deactivateReelSet()
            : this.reelsSetsHashMap[index].activateReelSet();
        }
      }
    }
    if (instantiated) this.addGameListeners();
  }

  update(delta: number): void {
    for (const reelSetKey in this.reelsSetsHashMap) {
      this.reelsSetsHashMap[reelSetKey].update(delta);
    }
  }

  onFsmStateChanged(state: GameStates) {
    switch (state) {
      case GameStates.Menu:
        this.onMenu();
        break;
      case GameStates.InitRound:
        this.onInitSpin();
        break;
      case GameStates.PerformRound:
        this.onSpin();
        break;
      case GameStates.PreStopRound:
        this.onPreStopSpin();
        break;
      case GameStates.Stop:
        this.onStop();
        break;
      case GameStates.Ready:
        this.onReady();
        break;
    }
  }
  //#endregion

  // #region Current Game Animation and Effects
  private titleElasticAnim(): void {
    const audioId =
      Resources.audios[audioSpriteSheetRes.sfx_game].audio.play('sfx_welcome');
    Resources.audios[audioSpriteSheetRes.sfx_game].audio.volume(0.4, audioId);
    TweenMax.to(this.gameTitle.scale, 0.1, {
      x: 0.8,
      y: 0.6,
      ease: Linear.easeNone,
      onComplete: () => {
        TweenMax.to(this.gameTitle.scale, 0.2, {
          x: 0.6,
          y: 0.8,
          ease: Linear.easeNone,
          onComplete: () => {
            TweenMax.to(this.gameTitle.scale, 0.2, {
              x: 0.75,
              y: 0.68,
              ease: Linear.easeNone,
              onComplete: () => {
                TweenMax.to(this.gameTitle.scale, 0.1, {
                  x: 0.7,
                  y: 0.7,
                  ease: Linear.easeNone,
                  onComplete: () => {
                    this.particleEffectTitle(this.gameTitle);
                    this.particleEffectGoldPot(
                      new PIXI.Point(this.gameTitle.width / 2 + 90, -5),
                      this.gameTitle
                    );
                    this.particleEffectGoldPot(
                      new PIXI.Point(-this.gameTitle.width / 2 - 130, -5),
                      this.gameTitle
                    );
                    this.playTitleSpinAnimation();
                  },
                });
              },
            });
          },
        });
      },
    });
  }

  /**
   * Recursive function to play title animation.
   * In case of Win animation is triggered in other part of code
   * We should stop the execution of this function
   * @private
   * @memberof Tropical
   */
  private playTitleSpinAnimation(): void {
    this.gameTitle.state.setAnimation(0, 'logo_idle', false);
    this.playingLogoIdle = true;
    setTimeout(() => {
      if (!this.playingLogoWin) this.playTitleSpinAnimation();
      else if (this.playingLogoIdle) this.playingLogoIdle = false;
    }, 3000);
  }

  private playTitleWinSpinAnimation(): void {
    this.playingLogoWin = true;
    this.gameTitle.state.setAnimation(0, 'logo_win', false);
    this.gameTitle.state.addListener({
      complete: () => {
        let ticker = new PIXI.ticker.Ticker();
        ticker.add((deltaTime) => {
          if (!this.playingLogoIdle) {
            ticker.stop();
            this.playingLogoWin = false;
            this.gameTitle.state.clearListeners();
            this.playTitleSpinAnimation();
          }
        });
        ticker.start();
      },
    });
  }

  private particleEffectTitle(container: PIXI.Container) {
    var emitter = new particles.Emitter(
      container,
      [imagesRes.smokeparticle, imagesRes.particle],
      this.titleParticleConfig
    );

    var elpsedEmitter = Date.now();
    let tickerParticle = new PIXI.ticker.Ticker();
    tickerParticle.add((deltaTime) => {
      var now = Date.now();
      emitter.update((now - elpsedEmitter) * 0.001);
      elpsedEmitter = now;
    });
    emitter.emit = true;
    tickerParticle.start();
  }

  private particleEffectGoldPot(
    postion: PIXI.Point,
    container: PIXI.Container
  ) {
    this.titleGoldPotParticleConfig.spawnRect.x = postion.x;
    this.titleGoldPotParticleConfig.spawnRect.y = postion.y;
    var emitter = new particles.Emitter(
      container,
      [imagesRes.particle],
      this.titleGoldPotParticleConfig
    );

    var elpsedEmitter = Date.now();
    let tickerParticle = new PIXI.ticker.Ticker();
    tickerParticle.add((deltaTime) => {
      var now = Date.now();
      emitter.update((now - elpsedEmitter) * 0.001);
      elpsedEmitter = now;
    });
    emitter.emit = true;
    tickerParticle.start();
  }

  private fadeOutCurrentReelsSet(): void {
    let reelsToDestroy: ReelSetInstanceMap = {};
    for (const reelSetKey in this.reelsSetsHashMap) {
      reelsToDestroy[reelSetKey] = this.reelsSetsHashMap[reelSetKey];
      this.midContainer.removeChild(reelsToDestroy[reelSetKey]);
      this.frontContainer.addChild(reelsToDestroy[reelSetKey]);
      TweenMax.to(reelsToDestroy[reelSetKey], 0.8, {
        alpha: 0,
        onComplete: () => {
          reelsToDestroy[reelSetKey].destroy();
        },
      });
    }
  }
  // #endregion

  // #region Current Game Logics
  onMenu(): void {}

  public startSpin(callback?: Function) {
    if (SlotMachine.fsmState === GameStates.Ready) {
      GcwEventsController.emitGcwEvent(
        gcw.api.GameEventType.TOTAL_BET_UPDATE,
        ExternalSlotController.getInstance().getParameter('bet') *
          ExternalSlotController.getInstance().getParameter(
            'totalBetMultiplier'
          )
      );
      GcwEventsController.subscribeForGcwEvent(
        gcw.api.GameEventType.ROUND_REQUEST_CONFIRMED,
        () => {
          SlotMachine.changeState(GameStates.InitRound);
        }
      );
      GcwEventsController.emitGcwEvent(gcw.api.GameEventType.ROUND_REQUEST);

      if (callback) {
        callback();
      }
    }
  }

  onInitSpin(): void {
    App.debugger.info(`Pre Spin Started`);
    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.ROUND_START);
    GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      elements: stateConfig['SPINNING'],
    });

    GcwEventsController.emitGcwEvent(
      gcw.api.GameEventType.CURRENCY_FORMATTING_RESPONSE,
      {
        prefix: '€',
        suffix: '',
        decimalPlaces: 2,
        decimalSeparator: '.',
        groupingSeparator: ',',
        prefixSeparator: '',
        suffixSeparator: '',
      }
    );

    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.WALLET_UPDATE, {
      balance: 996,
      free_balance: 0,
    });

    for (const reelSetKey in this.reelsSetsHashMap) {
      this.reelsSetsHashMap[reelSetKey].preStartSpin();
    }
  }

  onSpin(): void {
    App.debugger.info(`Spin Started`);

    const multipleReelSet = Object.keys(this.reelsSetsHashMap).length > 1;

    let delay = multipleReelSet
      ? SlotMachineSettings.ReelsStopDelayMultipleSets
      : SlotMachineSettings.ReelsStopDelaySingleSet;
    let timeOut = multipleReelSet
      ? SlotMachineSettings.ReelsDurationMultipleSets
      : SlotMachineSettings.ReelsDurationSingleSet;

    if (model.state.isFastSpinEnabled) {
      delay = 0;
      timeOut = 0;
    }

    for (const reelSetKey in this.reelsSetsHashMap) {
      this.reelsSetsHashMap[reelSetKey].startSpin();
    }

    GcwEventsController.subscribeForGcwEvent(
      gcw.api.GameEventType.NGS_RESPONSE,
      (response: any) => {
        let results = response.game.events.filter((event: any) => {
          return event.type === 'evt_rotate';
        });

        for (
          let indexReelSet = 0;
          indexReelSet < Object.keys(this.reelsSetsHashMap).length;
          indexReelSet++
        ) {
          let reelSetResult: ReelResult = {};
          for (
            let reelIndex = 0;
            reelIndex < results[indexReelSet].reels.length;
            reelIndex++
          ) {
            const letters = results[indexReelSet].reels[reelIndex].split('');
            let numbers: number[] = [];
            letters.forEach((letter: string) => {
              numbers.push(ReelsSymbolsCodes[letter]);
            });

            reelSetResult[reelIndex + 1] = numbers;

            App.debugger.success(
              `ReelSetID = ${indexReelSet + 1} ReelNumber ${reelIndex + 1}` +
                ` receive result ${results[indexReelSet].reels[reelIndex]} from the server` +
                ` numbers ${numbers}`
            );
          }
          setTimeout(() => {
            this.reelsSetsHashMap[indexReelSet + 1].preStopSpin(
              reelSetResult,
              delay
            );
          }, timeOut);
        }

        SlotMachine.changeState(GameStates.PreStopRound);
      }
    );
    const denom =
      ExternalSlotController.getInstance().getParameter('bet') *
      ExternalSlotController.getInstance().getParameter('totalBetMultiplier') *
      1000;
    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.NGS_REQUEST, {
      id: 0,
      date: new Date(),
      game: {
        input: {
          event: {
            type: 'evt_btnpush_conf',
            button: 'btn_start',
            lines: 10,
            betpl: 1,
            // totalBet = lines * betpl * denom / 10000
            denom,
            // used to identify specific calculations
            bmode: 35,
          },
          currentWallet: {
            currencySymbol: '€',
            currencyCode: 'EUR',
            decimalSpaces: 2,
            rm: 100,
            freeRounds: null,
            bonuses: [],
          },
        },
      },
      jackpot: {
        init: false,
        timestamp: new Date(),
      },
    });
  }

  onPreStopSpin(): void {
    App.debugger.info(`Pre Stop Spin Started`);
  }

  onStop(): void {
    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.RESULTS_SHOWN);
    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.ROUND_END);
    GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
      elements: stateConfig.IDLE,
    });
    ExternalSlotController.getInstance().updateModelWalletToLastState();
    App.debugger.info(`Round Finished.`);
  }

  onReady(): void {
    App.debugger.info(`Slot Machine Ready`);
  }

  private onChangeAmountOfReelsSets(newAmount: number): void {
    App.debugger.info(`Change amount of ReelSets to ${newAmount}`);
    this.setupReelsSet(newAmount);
  }
  private onReelSetPerformedPreStart(): void {
    const reelsSetPerformingPreStart = Object.keys(
      this.reelsSetsHashMap
    ).filter((key: any) => {
      return (
        this.reelsSetsHashMap[key].status === ReelSetStatus.PerformingPreStart
      );
    });

    if (reelsSetPerformingPreStart.length <= 0) {
      SlotMachine.changeState(GameStates.PerformRound);
    }
  }
  private onReelSetCompleted(): void {
    const reelsSetSpinning = Object.keys(this.reelsSetsHashMap).filter(
      (key: any) => {
        return this.reelsSetsHashMap[key].status === ReelSetStatus.Spinning;
      }
    );

    if (reelsSetSpinning.length <= 0) {
      this.playTitleWinSpinAnimation();

      this.coinAnimation.visible = true;
      // play coin animation from frame 0
      this.coinAnimation.gotoAndPlay(0);
      const soundId =
        Resources.audios[audioSpriteSheetRes.sfx_game].audio.play('sfx_money');
      this.coinAnimation.onComplete = () => {
        this.coinAnimation.visible = false;
        Resources.audios[audioSpriteSheetRes.sfx_game].audio.stop(soundId);
      };
      SlotMachine.changeState(GameStates.Stop);
    }
  }
  // #endregion
}
