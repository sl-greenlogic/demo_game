/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { atlasRes, loadedFiles } from '../settings/game.resources';
import { ScreenSettings, GameEvents } from '../settings/game.settings';
import { ReelInstanceMap, TropicalReel } from './tropicalReel';
import { ReelSet, ReelSetStatus } from '../sdk/core/gametypes/slotgame/reelSet';
import { SlotMachineSettings } from '../settings/slotmachine.settings';
import { ReelStatus, ReelResult } from '../sdk/core/gametypes/slotgame/reel';
import { GcwEventsController } from '../sdk/core/gcw/gcwEventsController';

export type ReelSetInstanceMap = { [key: number]: TropicalReelSet };

export class TropicalReelSet extends ReelSet {
  private backContainer!: PIXI.Container;
  private midContainer!: PIXI.Container;
  private frontContainer!: PIXI.Container;

  private readonly maskDimension = { width: 830, height: 410 };
  private reelsHashMap!: ReelInstanceMap;

  constructor(
    public reelSetId: number,
    private defaultPosition: PIXI.Point,
    defaultScale = 1,
    private activated = true,
    private reelSetVolume = 1
  ) {
    super();

    this.setupReelSetLayers();
    this.setupReelSetBackground();
    this.setupReelSetForeground();
    this.setupReels();
    this.addGameListeners();

    this.scale.set(defaultScale);
    this.position.set(
      ScreenSettings.Center.x + this.defaultPosition.x,
      ScreenSettings.Center.y + this.defaultPosition.y
    );

    window.addEventListener('resize', this.onResize.bind(this));
    if (window.innerHeight > window.innerWidth) {
      // is portrait
      this.onResize();
    }
  }
  public static getDeviceType = (): 'tablet' | 'mobile' | 'desktop' => {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 'tablet';
    }
    if (
      /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua
      )
    ) {
      return 'mobile';
    }
    return 'desktop';
  };

  private onResize() {
    const ration: number = window.innerWidth / window.innerHeight;
    if (window.innerHeight > window.innerWidth) {
      if (ration >= 0.7 && ration <= 1) {
        GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
          general: { useLayoutSet: 4 },
        });
      } else if (ration < 0.75 && ration > 0.5) {
        GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
          general: { useLayoutSet: 2 },
        });
      } else if (ration <= 0.5) {
        GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
          general: { useLayoutSet: 3 },
        });
      }
      this.scale.set(0.35);
      // this.frontContainer.children[0].scale.set(0.35);
      this.position.set(ScreenSettings.Center.x + this.defaultPosition.x, 150);
    } else {
      const device = TropicalReelSet.getDeviceType();
      this.scale.set(1);
      this.position.set(
        ScreenSettings.Center.x + this.defaultPosition.x,
        ScreenSettings.Center.y + this.defaultPosition.y
      );
      switch (device) {
        case 'desktop':
          return GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
            general: { useLayoutSet: 0 },
          });
        case 'mobile':
        case 'tablet':
          return GcwEventsController.emitGcwEvent('UPDATE_MODEL', {
            general: { useLayoutSet: 1 },
          });
      }
    }
  }
  // #region Interface Implementations
  setupReelSetLayers(): void {
    this.backContainer = new PIXI.Container();
    this.addChild(this.backContainer);
    this.midContainer = new PIXI.Container();
    this.addChild(this.midContainer);
    this.frontContainer = new PIXI.Container();
    this.addChild(this.frontContainer);
  }
  setupReelSetBackground(): void {
    const reelSetBg = new PIXI.Graphics();
    reelSetBg
      .beginFill(0)
      .drawRect(
        -this.maskDimension.width / 2,
        -this.maskDimension.height / 2,
        this.maskDimension.width,
        this.maskDimension.height
      )
      .endFill();
    this.backContainer.addChild(reelSetBg);
  }

  setupReelSetForeground(): void {
    const foreground = new PIXI.Sprite(
      loadedFiles[atlasRes.game_parts].textures['reel_frame.png']
    );
    foreground.anchor.set(0.5);
    foreground.position.set(0.5);
    this.frontContainer.addChild(foreground);
  }

  addGameListeners(): void {
    for (const reelKey in this.reelsHashMap) {
      this.reelsHashMap[reelKey].on(
        GameEvents.reelSpinCompleted,
        this.onReelSpinComplete.bind(this)
      );
      this.reelsHashMap[reelKey].on(
        GameEvents.reelPreSpinCompleted,
        this.onReelPreSpinComplete.bind(this)
      );
    }
  }

  setupReels(): void {
    const reelSetMask = new PIXI.Graphics();
    reelSetMask
      .beginFill(0)
      .drawRect(
        -this.maskDimension.width / 2,
        -this.maskDimension.height / 2,
        this.maskDimension.width,
        this.maskDimension.height
      )
      .endFill();
    this.midContainer.addChild(reelSetMask);

    const reelsContainer = new PIXI.Container();
    this.midContainer.addChild(reelsContainer);
    reelsContainer.mask = reelSetMask;

    let xOffset = -SlotMachineSettings.ReelsDistance * 2;
    // First Reel Position
    let yPosition = 0;

    this.reelsHashMap = [];
    for (let index = 1; index <= SlotMachineSettings.NumberOfReels; index++) {
      let reelPosition = new PIXI.Point(xOffset, yPosition);
      const reel = new TropicalReel(
        reelPosition,
        index + (this.reelSetId - 1) * SlotMachineSettings.NumberOfReels,
        this.activated,
        index,
        this.reelSetVolume
      );
      this.reelsHashMap[index] = reel;
      xOffset += SlotMachineSettings.ReelsDistance;
      reelsContainer.addChild(reel);
    }
  }

  preStartSpin(): void {
    this.status = ReelSetStatus.PerformingPreStart;
    for (const reelKey in this.reelsHashMap) {
      this.reelsHashMap[reelKey].preStartSpin();
    }
  }

  startSpin(): void {
    this.status = ReelSetStatus.Spinning;
    for (const reelKey in this.reelsHashMap) {
      this.reelsHashMap[reelKey].startSpin();
    }
  }

  preStopSpin(result: ReelResult, delay: number): void {
    for (const reelKey in this.reelsHashMap) {
      this.reelsHashMap[reelKey].preStopSpin(result[reelKey], delay);
    }
  }
  stopSpin(): void {
    // throw new Error("Method not implemented.");
  }

  update(delta: number): void {
    if (this.status === ReelSetStatus.Spinning) {
      for (const reelKey in this.reelsHashMap) {
        this.reelsHashMap[reelKey].update(delta);
      }
    }
  }
  // #endregion

  // #region Current Game Animation and Effects

  // #endregion

  // #region Current Game Logics
  get isActive(): boolean {
    return this.activated;
  }

  public activateReelSet(): void {
    if (!this.activated) {
      this.activated = true;
      for (const reelKey in this.reelsHashMap) {
        this.reelsHashMap[reelKey].activateReel();
      }
    }
  }

  public deactivateReelSet(): void {
    if (this.activated) {
      this.activated = false;
      for (const reelKey in this.reelsHashMap) {
        this.reelsHashMap[reelKey].deactivateReel();
      }
    }
  }

  private onReelPreSpinComplete(): void {
    const reelsSpinning = Object.keys(this.reelsHashMap).filter((key: any) => {
      return this.reelsHashMap[key].status === ReelStatus.StartingSpinning;
    });

    if (reelsSpinning.length <= 0) {
      this.status = ReelSetStatus.PreStartCompleted;
      this.emit(GameEvents.reelSetPerformedPreStart);
    }
  }

  private onReelSpinComplete(): void {
    const reelsSpinning = Object.keys(this.reelsHashMap).filter((key: any) => {
      return this.reelsHashMap[key].status === ReelStatus.Spinning;
    });

    if (reelsSpinning.length <= 0) {
      this.status = ReelSetStatus.ReadyToStart;
      this.emit(GameEvents.reelSetSpinCompleted);
    }
  }
  // #endregion
}
