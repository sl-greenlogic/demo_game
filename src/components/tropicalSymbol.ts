/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { ReelSymbol } from '../sdk/core/gametypes/slotgame/reelSymbol';
import { ReelsSymbols } from '../settings/slotmachine.settings';
import { loadedFiles, atlasRes } from '../settings/game.resources';

export class TropicalSymbol extends ReelSymbol {
  constructor(
    protected symbolId: number,
    defaultPosition: PIXI.Point,
    startActivated = true
  ) {
    super();
    this.anchor.set(0.5);
    this.position.set(defaultPosition.x, defaultPosition.y);
    this.updateSymbol(false);
    if (!startActivated) this.setInactiveColor();
  }

  public setInactiveColor(): void {
    this.tint = 0x666666;
  }

  public setActiveColor(): void {
    this.tint = 0xffffff;
  }

  public updateSymbol(
    startBlurred: boolean = true,
    newSymbolId?: number
  ): void {
    if (newSymbolId) this.symbolId = newSymbolId;

    if (startBlurred)
      this.texture =
        loadedFiles[atlasRes.symbols].textures[
          ReelsSymbols[this.symbolId].blurredSprite
        ];
    else
      this.texture =
        loadedFiles[atlasRes.symbols].textures[
          ReelsSymbols[this.symbolId].normalSprite
        ];
  }
}
