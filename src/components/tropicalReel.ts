/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { TropicalSymbol } from './tropicalSymbol';
import { Resources, audioSpriteSheetRes } from '../settings/game.resources';
import { TweenMax, Linear, Expo } from 'gsap';
import { SlotMachine, GameStates } from '../sdk/core/slotmachine';
import { Utils } from '../sdk/core/stakeUtils';
import { Reel, ReelStatus } from '../sdk/core/gametypes/slotgame/reel';
import {
  SlotMachineSettings,
  ReelsStrips,
} from '../settings/slotmachine.settings';
import { App } from '../app';
import { GameEvents } from '../settings/game.settings';
import { model } from '../model/SlotModel';

export type ReelInstanceMap = { [key: number]: TropicalReel };

export class TropicalReel extends Reel {
  private symbolsInstancesCount = 0;
  private symbolsDistance = 0;
  private symbolsHashMap: { [key: number]: TropicalSymbol } = {};
  // private spin = false;
  private finalSpin = false;
  private reelSpeed = 0;
  private reelReturnAnimDistance = 0;
  private bottomSymbolPosition = 0;
  private stopSound = '';

  constructor(
    defaultPosition: PIXI.Point,
    protected reelId: number,
    private activated: boolean,
    protected positionInReelSet: number,
    private soundVolume: number
  ) {
    super();

    this.symbolsInstancesCount =
      SlotMachineSettings.NumberOfVisibleSymbols +
      SlotMachineSettings.NumberOfHiddenSymbols;
    this.symbolsDistance = SlotMachineSettings.SymbolsDistance;
    this.reelReturnAnimDistance = SlotMachineSettings.ReelsReturnAnimDistance;
    // After middle visible symbol go 3 symbols down to get the bottom limit symbol Pos
    this.bottomSymbolPosition = this.symbolsDistance * 3;
    this.position.set(defaultPosition.x, defaultPosition.y);
    this.reelSpeed = SlotMachineSettings.ReelsSpeed;
    this.stopSound =
      SlotMachineSettings.ReelsStopSound[this.positionInReelSet - 1];
    this.stripInUse = [];

    this.setupReel();
  }

  // #region Interface Implementations
  setupReel(): void {
    let randomPosition = Utils.generateRandomNumber(
      0,
      ReelsStrips[this.positionInReelSet].length - 1,
      true
    );
    this.stripInUse = ReelsStrips[this.positionInReelSet].slice(randomPosition);
    this.stripInUse = this.stripInUse.concat(
      ReelsStrips[this.positionInReelSet]
    );

    for (let index = 0; index < this.symbolsInstancesCount; index++) {
      const yOffset = this.symbolsDistance * 2 - index * this.symbolsDistance;
      const position = new PIXI.Point(0, yOffset);
      const symbol = new TropicalSymbol(
        this.stripInUse[index],
        position,
        this.activated
      );
      this.addChild(symbol);
      this.symbolsHashMap[index + 1] = symbol;
    }
  }
  preStartSpin(): void {
    if (this.stripInUse.length === 0) this.resetInternalStrip();

    this.status = ReelStatus.StartingSpinning;
    this.reelImpulse();
  }
  startSpin(): void {
    this.status = ReelStatus.Spinning;
  }

  /**
   *
   *
   * @param {number[]} stopPositions Receive the visible symbols result
   * @param {number} delay
   * @memberof TropicalReel
   */
  preStopSpin(stopPositions: number[], delay: number): void {
    if (this.stripInUse.length < delay * this.reelId)
      this.stripInUse = this.stripInUse.concat(
        ReelsStrips[this.positionInReelSet]
      );

    this.stripInUse.splice((this.reelId - 1) * delay);

    console.log('STRIP FIRST STEP');
    console.log(this.stripInUse);

    //Clone to manipulate without using WHILE
    let reelStripClone = Object.assign([], ReelsStrips[this.positionInReelSet]);
    let newAdditionsList: number[] = [];
    Utils.randomizeList(reelStripClone);

    // in case the end of the reel was reached, add new symbols from the beginning of the strip
    newAdditionsList = newAdditionsList.concat(reelStripClone.splice(0, 5));

    console.log('STRIP SECOND STEP');
    console.log(newAdditionsList);

    // 2 random temporary values to be filled with unique new values that do not clumb in any way
    let randomValues: number[] = [];
    Utils.randomizeList(reelStripClone);
    randomValues = randomValues.concat(reelStripClone.splice(0, 2));

    this.stripInUse = this.stripInUse.concat(
      newAdditionsList,
      randomValues,
      stopPositions
    );
    this.finalSpin = true;

    App.debugger.info(
      'Final Strip ReelID ' + this.reelId + ' ' + this.stripInUse
    );
  }
  stopSpin(difference: number): void {
    this.status = ReelStatus.StoppingSpinning;
    this.finalSpin = false;

    const stopSound = Resources.audios[audioSpriteSheetRes.sfx_game].audio;
    stopSound.volume(this.soundVolume);
    stopSound.play(this.stopSound);

    for (
      let index = 0;
      index < Object.keys(this.symbolsHashMap).length;
      index++
    ) {
      const symbol = this.symbolsHashMap[index + 1];
      symbol.y -= difference;
      symbol.updateSymbol(false);
    }

    this.y += this.reelReturnAnimDistance;
    TweenMax.to(this, 0.3, {
      y: this.y - this.reelReturnAnimDistance,
      ease: Expo.easeOut,
    });

    this.status = ReelStatus.ReadyToStart;
    this.emit(GameEvents.reelSpinCompleted);
  }
  update(delta: number): void {
    if (this.status === ReelStatus.Spinning) {
      for (
        let index = 0;
        index < Object.keys(this.symbolsHashMap).length;
        index++
      ) {
        // Move Symbols
        const symbol = this.symbolsHashMap[index + 1];
        symbol.y += model.state.isFastSpinEnabled
          ? this.reelSpeed * 3
          : this.reelSpeed;
      }

      for (
        let index = 0;
        index < Object.keys(this.symbolsHashMap).length;
        index++
      ) {
        const symbol = this.symbolsHashMap[index + 1];
        // IF symbols position is bigger than first position send it to top of the symbols updated with new symbol definition
        if (
          symbol.y > this.bottomSymbolPosition &&
          this.stripInUse.length > 0
        ) {
          let symbolId: number | undefined = this.stripInUse.shift();
          if (symbolId) {
            symbol.updateSymbol(true, symbolId);
            symbol.y -=
              this.symbolsDistance * Object.keys(this.symbolsHashMap).length;
          }
        }
      }

      if (this.stripInUse.length === 0) {
        if (SlotMachine.fsmState === GameStates.PerformRound)
          this.resetInternalStrip();
        else if (this.finalSpin) {
          let readyToStop = false;
          let currentPos = 0;
          for (
            let index = 0;
            index < Object.keys(this.symbolsHashMap).length;
            index++
          ) {
            // find if any symbol is close enough of the last symbol position
            const symbol = this.symbolsHashMap[index + 1];
            if (symbol.y > this.bottomSymbolPosition) {
              readyToStop = true;
              currentPos = symbol.y;
            }
          }
          if (readyToStop) {
            const difference = currentPos - this.bottomSymbolPosition;
            this.stopSpin(difference);
          }
        }
      }
    }
  }
  // #endregion

  // #region Current Game Logics
  public activateReel(): void {
    if (!this.activated) {
      this.activated = true;
      for (const symbolKey in this.symbolsHashMap) {
        this.symbolsHashMap[symbolKey].setActiveColor();
      }
    }
  }
  public deactivateReel(): void {
    if (this.activated) {
      this.activated = false;
      for (const symbolKey in this.symbolsHashMap) {
        this.symbolsHashMap[symbolKey].setInactiveColor();
      }
    }
  }
  private reelImpulse(): void {
    TweenMax.to(this, 0.3, {
      y: this.y - 100,
      ease: Linear.easeOut,
      onComplete: () => {
        TweenMax.to(this, 0.1, {
          y: this.y + 100,
          ease: Expo.easeIn,
          onComplete: () => {
            this.status = ReelStatus.PreSpinCompleted;
            this.emit(GameEvents.reelPreSpinCompleted);
          },
        });
      },
    });
  }
  // #endregion
}
