export const DebugStyles = {
  Info: 'background: #272822; color: #ccc8bb;',
  Warn: 'background: #272822; color: #e2e22d;',
  Error: 'background: #272822; color: #ff1b1b;',
  Success: 'background: #272822; color: #72f93e;',
  Loaded: 'background: #272822; color: #66d9e2;',
};
