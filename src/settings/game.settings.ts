/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { BootScene } from '../scenes/boot.scene';
import { PreLoaderScene } from '../scenes/preloader.scene';
import { IntroScene } from '../scenes/intro.scene';
import { GameScene } from '../scenes/game.scene';
import { Tropical } from '../components/tropical';
import { GameControls } from '../componentsui/gameControls';

export const GAMENAME = 'SlotMachine';
/* Semantic Versioning
[MAJOR] version for "major" changes
[MINOR] version for add functionality in a backwards-compatible manner
[PATCH] version for make backwards-compatible bug fixes  */

export const GAMEVERSION = 'v2.1.0';

export const GameSettings = {
  ShowIntro: false,
  BackgroundSound: true,
  SoundEffects: true,
  GameClass: Tropical,
  GameControlClass: GameControls,
  Debug: true,
};

export const GameEvents = {
  reelSpinCompleted: 'reelSpinCompleted',
  reelPreSpinCompleted: 'reelPreSpinCompleted',
  reelSetSpinCompleted: 'reelSetSpinCompleted',
  reelSetPerformedPreStart: 'reelSetPerformedPreStart',
  reelSetReadyToStop: 'reelSetReadyToStop',
};

export const ControlEvents = {
  changeAmountOfReels: 'changeAmountOfReels',
};

export const NetworkEvents = {};

export const ScreenSettings = {
  Size: { width: 1440, height: 720 },
  AllowLandscape: true,
  Center: { x: 1440 / 2, y: 720 / 2 },
};

export const Scenes = {
  BootScene: BootScene,
  PreLoaderScene: PreLoaderScene,
  IntroScene: IntroScene,
  GameScene: GameScene,
};
