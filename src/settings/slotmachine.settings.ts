/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { TropicalReelSet } from '../components/tropicalReelSet';

export const SlotMachineSettings = {
  QuattroInstancesFromStart: 1,
  SingleReelSetPosition: new PIXI.Point(0, -26),
  MultipleReelSetPositions: [
    new PIXI.Point(-240, -130),
    new PIXI.Point(240, -130),
    new PIXI.Point(-240, 90),
    new PIXI.Point(240, 90),
  ],
  // MultipleReelSet1Position: new Phaser.Math.Vector2(-226, -146),
  // MultipleReelSet2Position: new Phaser.Math.Vector2(226, -146),
  // MultipleReelSet3Position: new Phaser.Math.Vector2(-226, 94),
  // MultipleReelSet4Position: new Phaser.Math.Vector2(226, 94),
  MultipleReelSetScale: 0.5,
  ReelsMaskVisible: true,
  ReelsSpeed: 32,
  ReelsOffsetX: -1, // compensation on the x-axis in case the reels are not exactly centered
  ReelsOffsetY: 198, // compensation to get to the botto, of the reels
  ReelsDistance: 165,
  ReelsStopDelaySingleSet: 3,
  ReelsStopDelayMultipleSets: 1,
  ReelsDurationSingleSet: 550,
  ReelsDurationMultipleSets: 550,
  ReelsStopSound: [
    'sfx_reelStop1',
    'sfx_reelStop2',
    'sfx_reelStop3',
    'sfx_reelStop4',
    'sfx_reelStop5',
  ],
  ReelsReturnAnimDistance: 50, //After stop the reel go down this distance to create tweenEffect
  NumberOfReels: 5,
  NumberOfVisibleSymbols: 3,
  NumberOfHiddenSymbols: 2,
  SymbolsDistance: 132,
  SymbolsWidth: 150,
  SymbolsHeight: 138,
};

export const SlotMachineComponents = {
  TropicalReelSet: TropicalReelSet,
};
export const ReelsStrips: ReelStrip = {
  1: [
    1, 8, 8, 8, 7, 7, 7, 3, 9, 9, 9, 6, 6, 6, 6, 6, 2, 2, 2, 2, 2, 6, 6, 6, 3,
    4, 4, 4, 1, 1, 1, 1, 4, 4, 4, 4, 2, 3, 5, 5, 5, 5, 7, 7, 7, 1, 9, 9, 9, 9,
    8, 8,
  ],
  2: [
    1, 5, 5, 5, 5, 5, 1, 2, 3, 4, 4, 4, 9, 9, 9, 2, 5, 5, 5, 5, 3, 1, 1, 1, 1,
    1, 2, 6, 6, 6, 7, 7, 7, 3, 3, 3, 6, 6, 6, 6, 9, 9, 9, 9, 9, 8, 8, 8, 1, 4,
    4, 4,
  ],
  3: [
    1, 1, 1, 1, 1, 3, 2, 2, 2, 2, 2, 5, 5, 5, 5, 5, 9, 9, 1, 6, 5, 5, 5, 5, 1,
    1, 1, 7, 7, 7, 2, 2, 2, 2, 1, 3, 2, 7, 7, 7, 7, 7, 1, 1, 1, 1, 4, 4, 4, 4,
    1, 3,
  ],
  4: [
    8, 8, 8, 1, 6, 6, 6, 6, 6, 2, 2, 2, 3, 2, 8, 8, 8, 1, 7, 7, 7, 2, 2, 2, 8,
    8, 8, 8, 9, 9, 9, 9, 9, 4, 4, 4, 8, 8, 8, 2, 5, 5, 5, 5, 5, 3, 3, 3, 2, 5,
    5, 5,
  ],
  5: [
    1, 2, 2, 2, 2, 2, 1, 3, 9, 9, 9, 9, 3, 4, 4, 4, 4, 3, 3, 3, 2, 8, 8, 8, 6,
    6, 6, 6, 1, 2, 4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 3, 5, 5, 5, 5, 4, 1, 3, 3, 3,
    3, 3,
  ],
};

export const ReelsSymbols: SymbolDefinitionsHashMap = {
  1: {
    normalSprite: 'sym_01_regular.png',
    blurredSprite: 'sym_01_blur.png',
    payOutValue: 0,
  },
  2: {
    normalSprite: 'sym_02_regular.png',
    blurredSprite: 'sym_02_blur.png',
    payOutValue: 0,
  },
  3: {
    normalSprite: 'sym_03_regular.png',
    blurredSprite: 'sym_03_blur.png',
    payOutValue: 0,
  },
  4: {
    normalSprite: 'sym_04_regular.png',
    blurredSprite: 'sym_04_blur.png',
    payOutValue: 0,
  },
  5: {
    normalSprite: 'sym_05_regular.png',
    blurredSprite: 'sym_05_blur.png',
    payOutValue: 0,
  },
  6: {
    normalSprite: 'sym_06_regular.png',
    blurredSprite: 'sym_06_blur.png',
    payOutValue: 0,
  },
  7: {
    normalSprite: 'sym_07_regular.png',
    blurredSprite: 'sym_07_blur.png',
    payOutValue: 0,
  },
  8: {
    normalSprite: 'sym_08_regular.png',
    blurredSprite: 'sym_08_blur.png',
    payOutValue: 0,
  },
  9: {
    normalSprite: 'sym_09_regular.png',
    blurredSprite: 'sym_09_blur.png',
    payOutValue: 0,
  },
};

export const ReelsSymbolsCodes: SymbolsCodeHashMap = {
  T: 1, //Ten
  J: 2, //J
  Q: 3, //Q
  K: 4, //K
  A: 5, //A
  B: 6, //blue gem
  G: 7, //green gem
  R: 8, //red gem
  L: 9, //Lucky Gem
};
type SymbolsCodeHashMap = {
  [letter: string]: number;
};

export type ReelStrip = {
  [key: number]: number[];
};

export type SymbolDefinitionsHashMap = {
  [key: number]: SymbolDefinition;
};
export type SymbolDefinition = {
  normalSprite: string;
  blurredSprite: string;
  payOutValue: number;
};
