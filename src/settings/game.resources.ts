/*!
 * @author       StakeLogic <info@stakelogic.com>
 * @copyright    2019 Stakelogic
 * @description  19SDK
 * @license      Stakelogic
 */

import { App } from '../app';
import { GcwEventsController } from '../sdk/core/gcw/gcwEventsController';
import { Howl } from 'howler';

export class Resources {
  static audios: LoadedAudiosHashMap;
  static animations: LoadedAnimationsHashMap;

  static loadResources(cbProgress: Function, cbComplete: Function): void {
    GcwEventsController.emitGcwEvent(gcw.api.GameEventType.PRELOADING_START);

    const assetsJson = PIXI.loader.resources['assets'].data;
    let audioSpritesKeys: AudioSprite[] = [];
    let animationSpritesKeys: SpriteSheetAnimation[] = [];
    Object.keys(assetsJson).forEach((group) => {
      Object.keys(assetsJson[group]).forEach((key) => {
        const value = assetsJson[group][key];

        if (
          group === 'atlas' ||
          group === 'image' ||
          group === 'spine' ||
          group === 'particle'
        ) {
          PIXI.loader.add(key, value);
        } else if (group === 'audiosprite') {
          PIXI.loader.add(key, value[0]);
          audioSpritesKeys.push({
            key: key,
            audiosFiles: value[1] as string[],
          });
        } else if (group === 'spritesheet') {
          PIXI.loader.add(key, value);
          animationSpritesKeys.push({
            key: key,
            spriteSheetFiles: value[1] as string[],
          });
        }
      });
    });

    let currentProgress = 0;
    PIXI.loader.on('progress', () => {
      currentProgress = PIXI.loader.progress;
      currentProgress = currentProgress / 1.5;
      cbProgress(currentProgress);
    });

    PIXI.loader.load(() => {
      loadedFiles = PIXI.loader.resources;
      App.debugger.loaded('Images resources loaded');
      this.loadAudios(
        currentProgress,
        audioSpritesKeys,
        animationSpritesKeys,
        cbProgress,
        cbComplete
      );
    });
  }

  private static loadAudios(
    currentProgress: number,
    audioSpritesKeys: AudioSprite[],
    animationSpritesKeys: SpriteSheetAnimation[],
    cbProgress: Function,
    cbComplete: Function
  ) {
    this.audios = {};
    this.animations = {};

    let ticker = new PIXI.ticker.Ticker();
    let audiosLoaded = 0;
    let animationsLoaded = 0;
    let animationsToLoad = animationSpritesKeys.length;
    const progressToComplete = 100 - currentProgress;
    const progressEachLoadedElement =
      progressToComplete /
      (audioSpritesKeys.length + animationSpritesKeys.length);

    ticker.add((deltaTime) => {
      if (animationsToLoad > 0) {
        animationSpritesKeys.forEach((spriteSheet) => {
          const spriteSheetJson = PIXI.loader.resources[spriteSheet.key].data;

          const frames: PIXI.Texture[] = [];
          Object.keys(spriteSheetJson['frames']).forEach((texture) => {
            frames.push(loadedFiles[spriteSheet.key].textures[texture]);
          });

          const anim = new PIXI.extras.AnimatedSprite(frames);
          this.animations[spriteSheet.key] = { animation: anim };

          animationsToLoad--;
          App.application.renderer.plugins.prepare.upload(anim, () => {
            animationsLoaded++;
            currentProgress += progressEachLoadedElement;
            cbProgress(currentProgress);
          });
        });
      }

      if (animationsLoaded === animationSpritesKeys.length) {
        audioSpritesKeys.forEach((sprite) => {
          const audioJson = PIXI.loader.resources[sprite.key].data;
          const properties: any = {};

          Object.keys(audioJson).forEach((group) => {
            Object.keys(audioJson[group]).forEach((key) => {
              if (group === 'sprite') {
                const value = audioJson[group][key];
                properties[key] = [value[0], value[1]];
              }
            });
          });

          const audio = new Howl({
            src: sprite.audiosFiles,
            sprite: properties,
          });

          this.audios[sprite.key] = { audio: audio };

          audio.once('load', () => {
            audiosLoaded++;
            if (audiosLoaded === audioSpritesKeys.length) {
              App.debugger.loaded(
                'Audios resources and Animation resources loaded'
              );
              ticker.stop();
              cbComplete(true);
            } else {
              currentProgress += progressEachLoadedElement;
              cbProgress(currentProgress);
            }
          });
        });
        ticker.stop();
      }
    });
    ticker.start();
  }
}

export const imagesRes = {
  background_main: 'background_main',
  background_introscreen: 'background_introscreen',
  placeholder_main: 'placeholder_main',
  particle: 'particle',
  smokeparticle: 'smokeparticle',
};
export const atlasRes = {
  ui_controls_buttons: 'ui_controls_buttons',
  introscreen: 'introscreen',
  game_parts: 'game_parts',
  symbols: 'symbols',
  winlines: 'winlines',
};
export const spriteSheetRes = { coins_anim: 'coins_anim' };
export const spineRes = {
  logo_spine: 'logo_spine',
  symbols_spine: 'symbols_spine',
};

export const audioSpriteSheetRes = {
  sfx_game: 'sfx_game',
  sfx_menu: 'sfx_menu',
  sfx_music: 'sfx_music',
};

export const animatedSpritesRes = { coins_anim: 'coins_anim' };

export const particlesRes = {
  title_glow: 'title_glow',
  title_goldPot: 'title_goldPot',
};

export let loadedFiles: any = {};

export interface AudioSprite {
  key: string;
  audiosFiles: string[];
}
export interface SpriteSheetAnimation {
  key: string;
  spriteSheetFiles: string[];
}

export type LoadedAudiosHashMap = {
  [key: string]: { audio: Howl };
};
export type LoadedAnimationsHashMap = {
  [key: string]: { animation: PIXI.extras.AnimatedSprite };
};
