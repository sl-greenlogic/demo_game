// window.onload = () => {

// for local testing - get EW from './EW/' (enrichedwrapper/dist/demoGame3/EW) - related to gameId=1091009601
let url =
  'https://ngp.dev02-gs-stakelogic.com/play?playerId=WRAPPER_DEMO_GAME&gameId=10910096&platform=desktop&gameJs=./build/gamename.js&thirdPartyEvent=true';

// get EW from server / CDN - related to gameId=10910096
// const url = "https://ngp.dev02-gs-stakelogic.com/play?playerId=svd1&gameId=10910096&platform=desktop&gameJs=./build/gamename.js";
const userAndPassword = btoa('demo:demo');

function init() {
  let loginRequest = new XMLHttpRequest();
  const newUrl = new URL(window.location.href);
  const playerId = newUrl.searchParams.get('playerId');
  if (playerId !== null) {
    url = url.replace('playerId=svd1', `playerId=${playerId}`);
  }
  loginRequest.open('GET', url, true);
  // loginRequest.setRequestHeader("Authorization", "Basic " + userAndPassword);
  loginRequest.onreadystatechange = () => {
    if (loginRequest.readyState === 4 && loginRequest.status === 200) {
      replaceHtmlTemplate(loginRequest.response);
    }
  };

  loginRequest.send();
}

function replaceHtmlTemplate(serverResponse) {
  // This is main script of ui
  // serverResponse = serverResponse.replace('</head>', '<link href="./EW/css/app.css" rel=stylesheet><script src="./EW/js/app.js"></script></head>');
  // serverResponse = serverResponse.replace('</head>', '<script src="http://cdn.dev02-gs-stakelogic.com/libs/stakelogic/ew/EW/js/app.js"></script></head>');

  // This replace methods accure us about correct paths
  // serverResponse = serverResponse.replace(/"\/css\//gi, '"./demo/css/');
  // serverResponse = serverResponse.replace(/"\/js\//gi, '"./demo/js/');
  // serverResponse = serverResponse.replace(/".\/build\//gi, '"./demo/build/');

  // console.error(document);

  document.open();
  document.write(serverResponse);
  document.close();
}

// window.onload = () => {
init();

// }
