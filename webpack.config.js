const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  devtool: 'eval-cheap-module-source-map',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'gamename.js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [path.resolve(path.join(__dirname, 'node_modules'))],
    fallback: {
      path: require.resolve('path-browserify'),
    },
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        options: { allowTsInNodeModules: true },
      },
    ],
  },
  devServer: {
    // host:'192.168.1.12',
    host: 'localhost',
    port: 8000,
  },

  performance: { hints: false },
};
