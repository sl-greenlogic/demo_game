// // };

// window.onload = function() {

gcw.gameElementsInit(gameEventListener);

function gameEventListener(type, data) {
  console.log('Game event (' + type + '): ' + JSON.stringify(data));
  if (type == gcw.api.GameEventType.HOME) {
    var hash = window.location.hash.split('#')[1];
    if (hash) {
      hash = decodeURIComponent(hash);
      if (!(hash.indexOf('http://') === 0 || hash.indexOf('https://') === 0)) {
        hash = 'http://' + hash;
      }
      console.log('Redirect: ' + hash);
      window.parent.location.href = hash;
    } else {
      console.log('No redirect defined. Performing history.back()');
      window.history.back();
    }
  }
  if (type == gcw.api.GameEventType.REGISTER) {
    var message = 'Event REGISTER: ' + data;
    console.log(message);
    alert(message);
  }
  if (type == gcw.api.GameEventType.CASHIER) {
    var message = 'Event CASHIER: ' + data;
    console.log(message);
    alert(message);
  }
  if (type == gcw.api.GameEventType.ERROR) {
    var message = 'Event ERROR: ' + data;
    console.log(message);
    alert(message);
  }
}

// };
